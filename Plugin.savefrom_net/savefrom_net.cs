﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharpBrowser.Plugins;

namespace Plugin.savefrom_net
{
    class savefrom_net : PluginAbstract
    {
        public override string PluginName => "savefrom.net 下载";
        public override void AfterAddressChanged(string address)
        {
            if (IsUrlMatch(address))
            {
                this.afterFormLoadArgs.BrowserImp.AddPluginItem(this.PluginName, (sender, args) =>
                {
                    address = "https://zh.savefrom.net/7/" + address;
                    this.afterFormLoadArgs.BrowserImp.LoadUrl(address);
                });
            }
        }

        private bool IsUrlMatch(string url)
        {
            var patterns = new string[]
            {
                ".*://.*.youtube.com/watch.*",
            };
            foreach (var pattern in patterns)
            {
                if (Regex.IsMatch(url, pattern))
                    return true;
            }

            return false;
        }
    }
}
