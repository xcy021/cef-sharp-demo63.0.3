﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace Plugin.m3u8
{
    interface IFrmInput
    {
        Dictionary<string, object> Input { get; set; }
        DialogResult ShowDialog();
        Dictionary<string, object> Output { get; set; }
    }
}