﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Plugin.m3u8
{
    public partial class FrmInput : Form, IFrmInput
    {
        public Dictionary<string, object> Input { get; set; }
        public Dictionary<string, object> Output { get; set; }

        public FrmInput()
        {
            InitializeComponent();
        }

        private void FrmInput_Load(object sender, EventArgs e)
        {
            if (Input != null)
            {
                this.textBox1.Text = Input["Text"].ToString();
                this.button1.Text = Input["Button"].ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var txt = this.textBox1.Text.Trim();
            if(string.IsNullOrWhiteSpace(txt))
            {
                MessageBox.Show("不能为空");
                return;
            }
            Output = new Dictionary<string, object>();
            Output.Add("Text", txt);
            this.DialogResult = DialogResult.OK;
        }
    }
}
