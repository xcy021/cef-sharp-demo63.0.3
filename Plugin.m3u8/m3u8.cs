﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharpBrowser;
using CefSharpBrowser.Plugins;

namespace Plugin.m3u8
{
    public class m3u8 : PluginAbstract
    {
        public override string PluginName { get; } = "m3u8";

        public override void AfterFormLoad(AfterFormLoadArgs args)
        {
            base.AfterFormLoad(args);
            InitializeM3u8SnifferPlugin(args);
        }

        public override void AfterAddressChanged(string address)
        {
            m3U8SnifferPlugin.ClearChildItems();
        }

        public override void BeforeResourceLoad(string url, int index)
        {
            base.BeforeResourceLoad(url, index);
            // m3u8插件处理
            if (url.ToLower().Contains("m3u8"))
            {
                m3U8SnifferPlugin.AddResM3U8(index + 1, url);
            }
        }

        #region m3u8嗅探器 插件
        private m3u8SnifferPlugin m3U8SnifferPlugin = new m3u8SnifferPlugin();

        private void InitializeM3u8SnifferPlugin(AfterFormLoadArgs args)
        {
            m3U8SnifferPlugin.AddOrUpdatePlugin += (item) =>
            {
                args.BrowserImp.AddPluginItem(item);
            };
        }

        class m3u8SnifferPlugin
        {
            private ToolStripMenuItem toolStripItemResM3U8 = new ToolStripMenuItem("m3u8嗅探器");

            public Action<ToolStripItem> AddOrUpdatePlugin;

            public m3u8SnifferPlugin()
            {
                toolStripItemResM3U8.Size = new System.Drawing.Size(98, 24);
            }

            public void AddResM3U8(int resOrder, string resUrl)
            {
                int c1 = 28;
                int c2 = 16;
                string str = resUrl;
                if (resUrl.Length > c1 + c2)
                {
                    str = resUrl.Substring(0, c1) + "..." + resUrl.Substring(resUrl.Length - c2, c2);
                }

                str = "[" + resOrder + "] " + str;
                var item = toolStripItemResM3U8.DropDownItems.Add(str, null, (sender, args) =>
                {
                    OpenResM3U8(resUrl);
                });
                toolStripItemResM3U8.DropDownItems.Insert(0, item);
                AddOrUpdatePlugin(toolStripItemResM3U8);
            }

            public void OpenResM3U8(string resUrl)
            {
                IFrmInput frmInput = new FrmInput();
                frmInput.Input = new Dictionary<string, object>()
                {
                    {"Text", resUrl},
                    {"Button", "下载"},
                };
                if (frmInput.ShowDialog() == DialogResult.OK)
                {
                    var result = frmInput.Output;
                    if (result.ContainsKey("Text"))
                    {
                        var urlNew = result["Text"].ToString();
                        Process.Start(new ProcessStartInfo()
                        {
                            FileName = GetNm3u8DLCLIFile(),
                            Arguments = urlNew,
                            WorkingDirectory = CefSharpBrowserRepo.GetPath(AppPath.Download)
                        });
                    }
                }
            }

            public void ClearChildItems()
            {
                toolStripItemResM3U8.DropDownItems.Clear();
            }

            public string GetNm3u8DLCLIFile()
            {
                return Path.Combine(CefSharpBrowserRepo.GetPath(AppPath.Tools), @"N_m3u8DL-CLI\N_m3u8DL-CLI.exe");
            }
        }
        #endregion
    }
}
