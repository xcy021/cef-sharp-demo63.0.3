﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using CefSharpBrowser;

namespace Plugin.JsLoader
{
    class JsLoaderManager
    {
        private static List<JsLoaderItem> jsLoaderItems = new List<JsLoaderItem>();

        static JsLoaderManager()
        {
            jsLoaderItems = Fetch();
        }

        private static DateTime lastWriteTime;

        private static bool IsChanged()
        {
            string jsLoaderDir = CefSharpBrowserRepo.GetPath(AppPath.JsLoader);
            if (!Directory.Exists(jsLoaderDir))
                return true;
            var files = Directory.GetFiles(jsLoaderDir);
            if (files.Length == 0)
                return true;
            DateTime time = files.ToList()
                .Select(file => File.GetLastWriteTime(file))
                .OrderBy(s => s)
                .LastOrDefault();
            return time != lastWriteTime;
        }

        private static List<JsLoaderItem> Fetch()
        {
            List<JsLoaderItem> result = new List<JsLoaderItem>();

            string jsLoaderDir = CefSharpBrowserRepo.GetPath(AppPath.JsLoader);
            if (!Directory.Exists(jsLoaderDir))
                Directory.CreateDirectory(jsLoaderDir);
            var files = Directory.GetFiles(jsLoaderDir);
            lastWriteTime = files.ToList()
                .Select(file => File.GetLastWriteTime(file))
                .OrderBy(s => s)
                .LastOrDefault();
            foreach (var file in files)
            {
                if (!file.ToLower().EndsWith(".js"))
                    continue;

                try
                {
                    var jsLoaderItem = AnalyzeFile(file);
                    result.Add(jsLoaderItem);
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"JsLoaderManager.Fetch() error:{ex.Message}" );
                }
            }

            return result;
        }

        private static JsLoaderItem AnalyzeFile(string file)
        {
            var lines = File.ReadAllLines(file, Encoding.GetEncoding("gb2312"));
            bool isFlagStartShown = false;
            bool isFlagEndShown = false;
            JsLoaderItem jsLoaderItem = new JsLoaderItem();
            jsLoaderItem.matchList = new List<string>();
            jsLoaderItem.requireList = new List<string>();
            jsLoaderItem.path = file;

            #region code
            foreach (var line in lines)
            {
                if (isFlagStartShown == false || isFlagEndShown == false)
                {
                    // 注释部分
                    if (line == "// ==UserScript==")
                    {
                        isFlagStartShown = true;
                    }
                    else if (line == "// ==/UserScript==")
                    {
                        isFlagEndShown = true;
                    }
                    else
                    {
                        if (line.StartsWith("//"))
                        {
                            // ==UserScript==
                            // @name         必应搜索广告去除
                            // @version      1.1
                            // @description  去除必应所有搜索结果形式的广告。
                            // @author       喝杯82年的Java压压惊
                            // @match        *://cn.bing.com/search*
                            // @match        *://www.bing.com/search*
                            // @grant        none
                            // @require      http://code.jquery.com/jquery-1.11.1.min.js
                            // @namespace 
                            // ==/UserScript==

                            var datas = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                            if (datas.Length >= 3)
                            {
                                var text = datas[1];
                                var datas2 = new string[datas.Length - 2];
                                Array.Copy(datas, 2, datas2, 0, datas2.Length);
                                var text2 = string.Join(" ", datas2);
                                switch (text)
                                {
                                    case "@name":
                                        jsLoaderItem.name = text2;
                                        break;
                                    case "@version":
                                        jsLoaderItem.version = text2;
                                        break;
                                    case "@description":
                                        jsLoaderItem.description = text2;
                                        break;
                                    case "@author":
                                        jsLoaderItem.author = text2;
                                        break;
                                    case "@require":
                                        jsLoaderItem.requireList.Add(text2);
                                        break;
                                    case "@match":
                                        jsLoaderItem.matchList.Add(text2);
                                        break;
                                    case "@menu":
                                        try
                                        {
                                            jsLoaderItem.menu = text2?.FromJson<List<MenuData>>();
                                        }
                                        catch (Exception ex)
                                        {
                                            Console.WriteLine($"@menu解析失败：{text2}");
                                        }
                                        break;
                                    case "@res":
                                        try
                                        {
                                            jsLoaderItem.res = text2?.FromJson<List<ResData>>();
                                        }
                                        catch (Exception ex)
                                        {
                                            Console.WriteLine($"@res解析失败：{text2}");
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    // 代码部分
                    jsLoaderItem.code += "\r\n" + line;
                }
            }
            #endregion

            return jsLoaderItem;
        }

        public static List<JsLoaderItem> GetMatchedJsLoaderItems(string url)
        {
            if (IsChanged())
                jsLoaderItems = Fetch();
            List<JsLoaderItem> result = new List<JsLoaderItem>();
            foreach (var jsLoaderItem in jsLoaderItems)
            {
                foreach (var match in jsLoaderItem.matchList)
                {
                    var pattern = match.Replace("*", ".*?");
                    if (Regex.IsMatch(url, pattern))
                    {
                        result.Add(jsLoaderItem);
                        break;
                    }
                }
            }

            return result;
        }
    }

    internal class JsLoaderItem
    {
        public string path;
        public string name;
        public string version;
        public string description;
        public string author;
        public List<string> matchList;
        public string grant;
        public List<string> requireList;
        public string @namespace;
        public string code;
        public List<MenuData> menu;
        public List<ResData> res;
    }

    internal class MenuData
    {
        public string title;
        public string click;
        public bool isChecked;
    }

    internal class ResData
    {
        public string ori;
        public string dst;
    }
}
