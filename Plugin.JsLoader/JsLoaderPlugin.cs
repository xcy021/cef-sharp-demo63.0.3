﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharpBrowser;
using CefSharpBrowser.Plugins;

namespace Plugin.JsLoader
{
    class JsLoaderPlugin : PluginAbstract
    {
        private string pluginName = "JsLoader";
        public override string PluginName => pluginName;
        private List<JsLoaderItem> found = new List<JsLoaderItem>();

        public override void AfterAddressChanged(string address)
        {
            if (IsUrlMatch(address))
            {
                var pluginItem = this.afterFormLoadArgs.BrowserImp.AddPluginItem(this.PluginName, null);
                LoadSubItems((ToolStripMenuItem)pluginItem);
            }
        }

        private void LoadSubItems(ToolStripMenuItem mainItem)
        {
            foreach (var jsLoaderItem in found)
            {
                string path = jsLoaderItem.path;
                var item = mainItem.DropDownItems.Add(jsLoaderItem.name, null, (sender, args) =>
                {
                    Process.Start("notepad.exe", jsLoaderItem.path);
                });

                // 替换资源
                if (jsLoaderItem.res != null)
                {
                    foreach (var res in jsLoaderItem.res)
                    {
                        var p = CefSharpBrowserRepo.GetPath(AppPath.JsLoader + "\\" + res.dst);
                        this.afterFormLoadArgs.BrowserImp.AddFileReplaceRule(res.ori,p);
                    }
                }
                // 添加子菜单
                if (jsLoaderItem.menu != null)
                {
                    var m = item as ToolStripMenuItem;
                    foreach (var menu in jsLoaderItem.menu)
                    {
                        var toolbar = new System.Windows.Forms.ToolStripMenuItem();
                        toolbar.Size = new System.Drawing.Size(224, 26);
                        toolbar.Tag = menu.click;
                        toolbar.Text = menu.title;
                        toolbar.Checked = menu.isChecked;
                        toolbar.Click += Toolbar_Click;

                        m.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { toolbar });
                    }
                }
            }

        }

        private void Toolbar_Click(object sender, EventArgs e)
        {
            var item = (sender as ToolStripMenuItem);
            var tag = item.Tag.ToString();
            item.Checked = !item.Checked;
            afterFormLoadArgs.BrowserImp.ExecuteJavaScriptAsync(tag);
        }

        public override void BeforeFrameLoadEnd(Action<string> executeJavaScriptAsyncAction, string address)
        {
            base.BeforeFrameLoadEnd(executeJavaScriptAsyncAction, address);
            // 根据网址地址加载对应的js
            var items = JsLoaderManager.GetMatchedJsLoaderItems(address);
            foreach (var jsLoaderItem in items)
            {
                executeJavaScriptAsyncAction(jsLoaderItem.code);
            }
        }

        private bool IsUrlMatch(string url)
        {
            found = JsLoaderManager.GetMatchedJsLoaderItems(url);
            if (found.Count > 0)
            {
                pluginName = "JsLoader(" + found.Count + ")";
                return true;
            }

            return false;
        }
    }
}
