﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharpBrowser.Plugins;

namespace Plugin.NSFW_youtube
{
    class NSFW_youtube : PluginAbstract
    {
        public override string PluginName => "NSFW youtube";
        public override void AfterAddressChanged(string address)
        {
            if (IsUrlMatch(address))
            {
                this.afterFormLoadArgs.BrowserImp.AddPluginItem(this.PluginName, (sender, args) =>
                {
                    address = address.Replace(".youtube.", ".nsfwyoutube.");
                    this.afterFormLoadArgs.BrowserImp.LoadUrl(address);
                });
            }
        }

        private bool IsUrlMatch(string url)
        {
            /*
             * https://www.youtube.com/watch?v=G9SyUeRt2ec   .*://.*.youtube.com/watch.*
             */

            var patterns = new string[]
            {
                ".*://.*.youtube.com/watch.*",
            };
            foreach (var pattern in patterns)
            {
                if (Regex.IsMatch(url, pattern))
                    return true;
            }

            return false;
        }
    }
}
