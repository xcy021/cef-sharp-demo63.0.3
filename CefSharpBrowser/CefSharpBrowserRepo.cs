﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using CefSharpBrowser.Handlers;
using CefSharpBrowser.Handlers.Initializer;

namespace CefSharpBrowser
{
    public static class CefSharpBrowserRepo
    {
        static CefSharpBrowserRepo()
        {
            InitDirectories();
        }

        public static IMyCefInitializer CreateCefInitializer()
        {
            return new MyCefInitializer();
        }

        public static IHandlerservice CreateHandlerService()
        {
            return new Handlerservice();
        }

        public static string GetPath(string name)
        {
            return Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), name);
        }

        private static void InitDirectories()
        {
            var dirs = new string[]
            {
                AppPath.Plugins,
                AppPath.JsLoader,
                AppPath.ResReplace,
                AppPath.Tools,
                AppPath.Download,
            };
            foreach (var dir in dirs)
            {
                if(Directory.Exists(dir))
                    continue;
                Directory.CreateDirectory(dir);
            }
        }

        public static string HomeUrl { get; set; } = "https://www.baidu.com";

        /// <summary>
        /// 是否允许开小号
        /// </summary>
        public static bool AllowMultiAccountCookie { get; set; }
    }

    public static class AppPath
    {
        public static string AppName { get; private set; }
        public static string AppDir { get; private set; }
        public static string Plugins { get; private set; }
        public static string JsLoader { get; private set; }
        public static string ResReplace { get; private set; }
        public static string Tools { get; private set; }
        public static string Download { get; private set; }

        static AppPath()
        {
            AppName = Path.GetFileName(Assembly.GetEntryAssembly().Location);
            AppDir = "";
            Plugins = "Plugins";
            JsLoader = "JsLoader";
            ResReplace = "ResReplace";
            Tools = "Tools";
            Download = "Download";
        }
    }
}
