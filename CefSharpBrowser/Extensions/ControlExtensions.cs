﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CefSharpBrowser.Extensions
{
    internal static class ControlExtensions
    {
        public static void RunOnUI(this Control ctrl, Action action)
        {
            if (ctrl.IsDisposedOrNotCreated())
                return;

            ctrl.Invoke(action);
        }
        public static void RunOnUI<T>(this Control ctrl, Action<T> action, T actionParameter)
        {
            if (ctrl.IsDisposedOrNotCreated())
                return;

            ctrl.Invoke(new Action(() => action(actionParameter)));
        }

        public static void RunOnUIAsync(this Control ctrl, Action action)
        {
            if (ctrl.IsDisposedOrNotCreated())
                return;

            ctrl.BeginInvoke(action);
        }

        public static void RunOnUIAsync<T>(this Control ctrl, Action<T> action, T actionParameter)
        {
            if (ctrl.IsDisposedOrNotCreated())
                return;

            ctrl.BeginInvoke(new Action(() => action(actionParameter)));
        }

        public static bool IsDisposedOrNotCreated(this Control ctrl)
        {
            if (ctrl.IsDisposed || !ctrl.Created)
                return true;
            return false;
        }
    }
}
