﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CefSharpBrowser.Extensions
{
    internal static class ToolStripTextBoxExtensions
    {
        public static void OnEnterPressed(this ToolStripTextBox c, Action<ToolStripTextBox> action)
        {
            c.KeyPress += (sender, e) =>
            {
                if (e.KeyChar == (char)Keys.Enter)
                {
                    action(sender as ToolStripTextBox);
                    e.Handled = true;
                }
            };
        }

        public static void OnEnterPressed(this TextBox c, Action<TextBox> action)
        {
            c.KeyPress += (sender, e) =>
            {
                if (e.KeyChar == (char)Keys.Enter)
                {
                    action(sender as TextBox);
                    e.Handled = true;
                }
            };
        }
    }
}
