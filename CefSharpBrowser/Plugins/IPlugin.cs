﻿using System;
using System.Windows.Forms;
using CefSharp;
using CefSharpBrowser.Browser;

namespace CefSharpBrowser.Plugins
{
    public class AfterFormLoadArgs
    {
        public IChromeWebBrowserUC BrowserImp { get; set; }
    }

    public interface IPlugin
    {
        string PluginName { get; }
        void AfterFormLoad(AfterFormLoadArgs args);
        void AfterAddressChanged(string address);
        void BeforeResourceLoad(string url, int index);
        void BeforeFrameLoadEnd(Action<string> executeJavaScriptAsyncAction, string address);
    }

    public abstract class PluginAbstract : IPlugin
    {
        protected AfterFormLoadArgs afterFormLoadArgs;

        public abstract string PluginName { get; }

        public virtual void AfterFormLoad(AfterFormLoadArgs args)
        {
            this.afterFormLoadArgs = args;
        }

        public abstract void AfterAddressChanged(string address);

        public virtual void BeforeResourceLoad(string url, int index)
        {
        }

        public virtual void BeforeFrameLoadEnd(Action<string> executeJavaScriptAsyncAction, string address)
        {
        }
    }
}