﻿
namespace CefSharpBrowser.Browser
{
    partial class ChromeWebBrowserUC
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChromeWebBrowserUC));
            this.buttonFindNextRes = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.textBoxResUrl = new System.Windows.Forms.TextBox();
            this.Column2 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.ToolStripMenuItemCloseResBar = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripStatusLabelState = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStripBottom = new System.Windows.Forms.StatusStrip();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripTextBoxUrl = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripButtonHome = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonRefresh = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonForward = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonBack = new System.Windows.Forms.ToolStripButton();
            this.toolStripDropDownButtonEnabledPlugins = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripTop = new System.Windows.Forms.ToolStrip();
            this.contextMenuStripBookmark = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextMenuStripRes = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.复制请求地址ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStripResFind = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.使用图片关键字ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.使用视频关键字ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.使用音频关键字ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.使用m3u8关键字ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.statusStripBottom.SuspendLayout();
            this.toolStripTop.SuspendLayout();
            this.contextMenuStripRes.SuspendLayout();
            this.contextMenuStripResFind.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonFindNextRes
            // 
            this.buttonFindNextRes.ContextMenuStrip = this.contextMenuStripResFind;
            this.buttonFindNextRes.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonFindNextRes.Location = new System.Drawing.Point(829, 0);
            this.buttonFindNextRes.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonFindNextRes.Name = "buttonFindNextRes";
            this.buttonFindNextRes.Size = new System.Drawing.Size(100, 36);
            this.buttonFindNextRes.TabIndex = 2;
            this.buttonFindNextRes.Text = "查找";
            this.buttonFindNextRes.UseVisualStyleBackColor = true;
            this.buttonFindNextRes.Click += new System.EventHandler(this.buttonFindNextRes_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.textBoxResUrl);
            this.panel2.Controls.Add(this.buttonFindNextRes);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 28);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(929, 36);
            this.panel2.TabIndex = 2;
            // 
            // textBoxResUrl
            // 
            this.textBoxResUrl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxResUrl.Location = new System.Drawing.Point(4, 4);
            this.textBoxResUrl.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxResUrl.Name = "textBoxResUrl";
            this.textBoxResUrl.Size = new System.Drawing.Size(816, 25);
            this.textBoxResUrl.TabIndex = 1;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "操作";
            this.Column2.MinimumWidth = 6;
            this.Column2.Name = "Column2";
            this.Column2.Width = 125;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "替换后";
            this.Column3.MinimumWidth = 6;
            this.Column3.Name = "Column3";
            this.Column3.Width = 125;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "请求";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            this.Column1.Width = 500;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "序号";
            this.Column4.MinimumWidth = 6;
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 80;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column4,
            this.Column1,
            this.Column3,
            this.Column2});
            this.dataGridView1.ContextMenuStrip = this.contextMenuStripRes;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 64);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(929, 125);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 31);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dataGridView1);
            this.splitContainer1.Panel2.Controls.Add(this.panel2);
            this.splitContainer1.Panel2.Controls.Add(this.menuStrip1);
            this.splitContainer1.Size = new System.Drawing.Size(929, 531);
            this.splitContainer1.SplitterDistance = 337;
            this.splitContainer1.SplitterWidth = 5;
            this.splitContainer1.TabIndex = 8;
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(929, 337);
            this.panel1.TabIndex = 3;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemCloseResBar});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(929, 28);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // ToolStripMenuItemCloseResBar
            // 
            this.ToolStripMenuItemCloseResBar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.ToolStripMenuItemCloseResBar.Name = "ToolStripMenuItemCloseResBar";
            this.ToolStripMenuItemCloseResBar.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this.ToolStripMenuItemCloseResBar.Size = new System.Drawing.Size(33, 24);
            this.ToolStripMenuItemCloseResBar.Text = "X";
            this.ToolStripMenuItemCloseResBar.Click += new System.EventHandler(this.ToolStripMenuItemCloseResBar_Click);
            // 
            // toolStripStatusLabelState
            // 
            this.toolStripStatusLabelState.Name = "toolStripStatusLabelState";
            this.toolStripStatusLabelState.Size = new System.Drawing.Size(39, 20);
            this.toolStripStatusLabelState.Text = "状态";
            // 
            // statusStripBottom
            // 
            this.statusStripBottom.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStripBottom.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelState});
            this.statusStripBottom.Location = new System.Drawing.Point(0, 562);
            this.statusStripBottom.Name = "statusStripBottom";
            this.statusStripBottom.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStripBottom.Size = new System.Drawing.Size(929, 26);
            this.statusStripBottom.TabIndex = 6;
            this.statusStripBottom.Text = "statusStrip1";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
            // 
            // toolStripTextBoxUrl
            // 
            this.toolStripTextBoxUrl.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F);
            this.toolStripTextBoxUrl.Name = "toolStripTextBoxUrl";
            this.toolStripTextBoxUrl.Size = new System.Drawing.Size(399, 31);
            this.toolStripTextBoxUrl.ToolTipText = "地址";
            // 
            // toolStripButtonHome
            // 
            this.toolStripButtonHome.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonHome.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonHome.Image")));
            this.toolStripButtonHome.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonHome.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.toolStripButtonHome.Name = "toolStripButtonHome";
            this.toolStripButtonHome.Size = new System.Drawing.Size(29, 28);
            this.toolStripButtonHome.Tag = "home";
            this.toolStripButtonHome.Text = "首页";
            this.toolStripButtonHome.Click += new System.EventHandler(this.toolStripButtonNavTools_Click);
            // 
            // toolStripButtonRefresh
            // 
            this.toolStripButtonRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonRefresh.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonRefresh.Image")));
            this.toolStripButtonRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRefresh.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.toolStripButtonRefresh.Name = "toolStripButtonRefresh";
            this.toolStripButtonRefresh.Size = new System.Drawing.Size(29, 28);
            this.toolStripButtonRefresh.Tag = "refresh";
            this.toolStripButtonRefresh.Text = "刷新";
            this.toolStripButtonRefresh.Click += new System.EventHandler(this.toolStripButtonNavTools_Click);
            // 
            // toolStripButtonForward
            // 
            this.toolStripButtonForward.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonForward.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonForward.Image")));
            this.toolStripButtonForward.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonForward.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.toolStripButtonForward.Name = "toolStripButtonForward";
            this.toolStripButtonForward.Size = new System.Drawing.Size(29, 28);
            this.toolStripButtonForward.Tag = "forward";
            this.toolStripButtonForward.Text = "前进";
            this.toolStripButtonForward.Click += new System.EventHandler(this.toolStripButtonNavTools_Click);
            // 
            // toolStripButtonBack
            // 
            this.toolStripButtonBack.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonBack.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonBack.Image")));
            this.toolStripButtonBack.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonBack.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.toolStripButtonBack.Name = "toolStripButtonBack";
            this.toolStripButtonBack.Size = new System.Drawing.Size(29, 28);
            this.toolStripButtonBack.Tag = "back";
            this.toolStripButtonBack.Text = "后退";
            this.toolStripButtonBack.Click += new System.EventHandler(this.toolStripButtonNavTools_Click);
            // 
            // toolStripDropDownButtonEnabledPlugins
            // 
            this.toolStripDropDownButtonEnabledPlugins.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButtonEnabledPlugins.Name = "toolStripDropDownButtonEnabledPlugins";
            this.toolStripDropDownButtonEnabledPlugins.Size = new System.Drawing.Size(98, 28);
            this.toolStripDropDownButtonEnabledPlugins.Text = "启用的插件";
            // 
            // toolStripTop
            // 
            this.toolStripTop.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStripTop.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonBack,
            this.toolStripButtonForward,
            this.toolStripButtonRefresh,
            this.toolStripButtonHome,
            this.toolStripTextBoxUrl,
            this.toolStripSeparator1,
            this.toolStripDropDownButtonEnabledPlugins});
            this.toolStripTop.Location = new System.Drawing.Point(0, 0);
            this.toolStripTop.Name = "toolStripTop";
            this.toolStripTop.Size = new System.Drawing.Size(929, 31);
            this.toolStripTop.TabIndex = 5;
            this.toolStripTop.Text = "toolStrip1";
            // 
            // contextMenuStripBookmark
            // 
            this.contextMenuStripBookmark.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStripBookmark.Name = "contextMenuStripBookmark";
            this.contextMenuStripBookmark.Size = new System.Drawing.Size(61, 4);
            // 
            // contextMenuStripRes
            // 
            this.contextMenuStripRes.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStripRes.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.复制请求地址ToolStripMenuItem});
            this.contextMenuStripRes.Name = "contextMenuStripRes";
            this.contextMenuStripRes.Size = new System.Drawing.Size(169, 28);
            this.contextMenuStripRes.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStripRes_Opening);
            // 
            // 复制请求地址ToolStripMenuItem
            // 
            this.复制请求地址ToolStripMenuItem.Name = "复制请求地址ToolStripMenuItem";
            this.复制请求地址ToolStripMenuItem.Size = new System.Drawing.Size(168, 24);
            this.复制请求地址ToolStripMenuItem.Text = "复制请求地址";
            this.复制请求地址ToolStripMenuItem.Click += new System.EventHandler(this.复制请求地址ToolStripMenuItem_Click);
            // 
            // contextMenuStripResFind
            // 
            this.contextMenuStripResFind.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStripResFind.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.使用图片关键字ToolStripMenuItem,
            this.使用视频关键字ToolStripMenuItem,
            this.使用音频关键字ToolStripMenuItem,
            this.使用m3u8关键字ToolStripMenuItem});
            this.contextMenuStripResFind.Name = "contextMenuStripResFind";
            this.contextMenuStripResFind.Size = new System.Drawing.Size(211, 128);
            // 
            // 使用图片关键字ToolStripMenuItem
            // 
            this.使用图片关键字ToolStripMenuItem.Name = "使用图片关键字ToolStripMenuItem";
            this.使用图片关键字ToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.使用图片关键字ToolStripMenuItem.Text = "使用图片关键字";
            this.使用图片关键字ToolStripMenuItem.Click += new System.EventHandler(this.使用图片关键字ToolStripMenuItem_Click);
            // 
            // 使用视频关键字ToolStripMenuItem
            // 
            this.使用视频关键字ToolStripMenuItem.Name = "使用视频关键字ToolStripMenuItem";
            this.使用视频关键字ToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.使用视频关键字ToolStripMenuItem.Text = "使用视频关键字";
            this.使用视频关键字ToolStripMenuItem.Click += new System.EventHandler(this.使用视频关键字ToolStripMenuItem_Click);
            // 
            // 使用音频关键字ToolStripMenuItem
            // 
            this.使用音频关键字ToolStripMenuItem.Name = "使用音频关键字ToolStripMenuItem";
            this.使用音频关键字ToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.使用音频关键字ToolStripMenuItem.Text = "使用音频关键字";
            this.使用音频关键字ToolStripMenuItem.Click += new System.EventHandler(this.使用音频关键字ToolStripMenuItem_Click);
            // 
            // 使用m3u8关键字ToolStripMenuItem
            // 
            this.使用m3u8关键字ToolStripMenuItem.Name = "使用m3u8关键字ToolStripMenuItem";
            this.使用m3u8关键字ToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.使用m3u8关键字ToolStripMenuItem.Text = "使用m3u8关键字";
            this.使用m3u8关键字ToolStripMenuItem.Click += new System.EventHandler(this.使用m3u8关键字ToolStripMenuItem_Click);
            // 
            // ChromeWebBrowserUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusStripBottom);
            this.Controls.Add(this.toolStripTop);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "ChromeWebBrowserUC";
            this.Size = new System.Drawing.Size(929, 588);
            this.Load += new System.EventHandler(this.ChromeWebBrowserUC_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStripBottom.ResumeLayout(false);
            this.statusStripBottom.PerformLayout();
            this.toolStripTop.ResumeLayout(false);
            this.toolStripTop.PerformLayout();
            this.contextMenuStripRes.ResumeLayout(false);
            this.contextMenuStripResFind.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonFindNextRes;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox textBoxResUrl;
        private System.Windows.Forms.DataGridViewButtonColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelState;
        private System.Windows.Forms.StatusStrip statusStripBottom;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxUrl;
        private System.Windows.Forms.ToolStripButton toolStripButtonHome;
        private System.Windows.Forms.ToolStripButton toolStripButtonRefresh;
        private System.Windows.Forms.ToolStripButton toolStripButtonForward;
        private System.Windows.Forms.ToolStripButton toolStripButtonBack;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButtonEnabledPlugins;
        private System.Windows.Forms.ToolStrip toolStripTop;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripBookmark;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemCloseResBar;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripRes;
        private System.Windows.Forms.ToolStripMenuItem 复制请求地址ToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripResFind;
        private System.Windows.Forms.ToolStripMenuItem 使用图片关键字ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 使用视频关键字ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 使用音频关键字ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 使用m3u8关键字ToolStripMenuItem;
    }
}
