﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using CefSharp;
using CefSharp.WinForms;
using CefSharpBrowser;
using CefSharpBrowser.Extensions;
using CefSharpBrowser.Handlers;
using CefSharpBrowser.Handlers.Resource;
using CefSharpBrowser.Plugins;

namespace CefSharpBrowser.Browser
{
    public interface IChromeWebBrowserUC
    {
        Form FindForm();

        /// <summary>
        /// 加载url
        /// </summary>
        /// <param name="url"></param>
        void LoadUrl(string url);

        /// <summary>
        /// 重新加载
        /// </summary>
        void Reload();

        void ShowDevTools();
        void ClearCookie();
        void ExecuteJavaScriptAsync(string url);
        void ShowResPanel(bool isShow);
        ToolStrip CreateAndInsertToolStrip();
        ToolStripDropDownButton CreateAndInsertDropDownButton(string title);
        void RegisterCommand(string name, Action action);
        void RunCommand(string name);
        void RegisterJsObject(string jsObjName, object csharpObj);
        void RunOnUI(Action action);
        void RunOnUIAsync(Action action);
        void AddPluginItem(ToolStripItem item);
        ToolStripItem AddPluginItem(string text, EventHandler onClick);
        void AddFileReplaceRule(string urlPattern, string localFile);
        void AddCookie(string url, System.Net.Cookie c);

        /// <summary>
        /// 获取浏览器url
        /// </summary>
        /// <returns></returns>
        string GetAddress();
    }

    public partial class ChromeWebBrowserUC : UserControl, IChromeWebBrowserUC
    {
        ChromiumWebBrowser _browser;
        IHandlerservice _Handlerservice = new Handlerservice();
        private List<IPlugin> _plugins = null;
        private Action<string> _addTabAction;
        private readonly Action _removeTabAction;
        private string _url;
        private Action<object, string> _changeTabTitle;

        #region IChromeWebBrowserUC

        /// <summary>
        /// 加载url
        /// </summary>
        /// <param name="url"></param>
        public void LoadUrl(string url)
        {
            if (!url.Contains("://"))
            {
                if (url.ToLower().EndsWith(".com") ||
                    url.ToLower().EndsWith(".cn") ||
                    url.ToLower().EndsWith(".net") ||
                    url.ToLower().EndsWith(".org") ||
                    url.ToLower().EndsWith(".gov"))
                {
                    url = "http://" + url;
                }
                else
                {
                    // 非网址时，自动调用百度搜索
                    url = "https://www.baidu.com/s?wd=" + System.Web.HttpUtility.UrlEncode(url);
                }
            }
            ChangeUIDataWhenLoad(url);
            _browser.Load(url);
        }

        /// <summary>
        /// 获取浏览器url
        /// </summary>
        /// <returns></returns>
        public string GetAddress()
        {
            return _browser.Address;
        }

        /// <summary>
        /// 重新加载
        /// </summary>
        public void Reload()
        {
            string url = this._browser.Address;
            ChangeUIDataWhenLoad(url);
            _browser.GetBrowser().Reload(true);
        }

        public void ShowDevTools()
        {
            _browser?.ShowDevTools();
        }

        public void ClearCookie()
        {
            _Handlerservice?.GetCookieManager()?.ClearCookie();
        }

        public void AddCookie(string url, System.Net.Cookie c)
        {
            var cc = new Cookie();
            cc.Name = c.Name;
            cc.Value = c.Value;
            cc.Expires = c.Expires;
            cc.Path = c.Path;
            _Handlerservice?.GetCookieManager()?.AddCookie(url, cc);
        }

        public void ExecuteJavaScriptAsync(string url)
        {
            _browser.GetBrowser().MainFrame.ExecuteJavaScriptAsync(url);
        }

        public void ShowResPanel(bool isShow)
        {
            this.splitContainer1.Panel2Collapsed = !isShow;
        }

        public ToolStrip CreateAndInsertToolStrip()
        {
            var toolStrip = new System.Windows.Forms.ToolStrip();
            toolStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            toolStrip.Location = new System.Drawing.Point(0, 31);
            toolStrip.Size = new System.Drawing.Size(697, 25);
            toolStrip.TabIndex = 7;
            this.Controls.Add(toolStrip);
            int index = this.Controls.IndexOf(toolStripTop);
            this.Controls.SetChildIndex(toolStrip, index);

            return toolStrip;
        }

        public ToolStripDropDownButton CreateAndInsertDropDownButton(string title)
        {
            var toolStripDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            toolStripDropDownButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            toolStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            toolStripDropDownButton.Size = new System.Drawing.Size(69, 28);
            toolStripDropDownButton.Text = title;
            this.toolStripTop.Items.Add(toolStripDropDownButton);

            return toolStripDropDownButton;
        }

        private Dictionary<string, Action> registeredCommands = new Dictionary<string, Action>();

        public void RegisterCommand(string name, Action action)
        {
            if (registeredCommands.ContainsKey(name))
                registeredCommands[name] = action;
            else
                registeredCommands.Add(name, action);
        }

        public void RunCommand(string name)
        {
            if (registeredCommands.ContainsKey(name))
                registeredCommands[name]();
        }

        public void RegisterJsObject(string jsObjName, object csharpObj)
        {
            _browser.Register(jsObjName, csharpObj, null);
        }

        public void RunOnUI(Action action)
        {
            if (this.IsHandleCreated)
                this.Invoke(action);
        }

        public void RunOnUIAsync(Action action)
        {
            if(this.IsHandleCreated)
                this.BeginInvoke(action);
        }

        public void AddPluginItem(ToolStripItem item)
        {
            var pluginCtrl = this.toolStripDropDownButtonEnabledPlugins;
            pluginCtrl.DropDownItems.Add(item);
            pluginCtrl.Text = "已启用的插件(" + pluginCtrl.DropDownItems.Count + "个)";
        }

        public ToolStripItem AddPluginItem(string text, EventHandler onClick)
        {
            return this.toolStripDropDownButtonEnabledPlugins.DropDownItems.Add(text, null, onClick);
        }

        public void AddFileReplaceRule(string urlPattern, string localFile)
        {
            var res = _browser.RequestHandler as IMyResReplaceRequestHandler;
            res?.AddFileReplaceRule(urlPattern, localFile);
        }

        #endregion

        public ChromeWebBrowserUC(string url, Action<string> addTabAction, Action removeTabAction, Action<object, string> changeTabTitle)
        {
            _changeTabTitle = changeTabTitle;
            _url = url;
            _addTabAction = addTabAction;
            _removeTabAction = removeTabAction;
            InitializeComponent();
            InitializePlugins();
            InitializeBrowser();
            InitializeResFinder();
            _plugins.ForEach(a=>a.AfterFormLoad(new AfterFormLoadArgs()
            {
                BrowserImp = this,
            }));
        }

        private void InitializePlugins()
        {
            _plugins = new List<IPlugin>();
            var pluginDir = CefSharpBrowserRepo.GetPath(AppPath.Plugins);
            if (!Directory.Exists(pluginDir))
                Directory.CreateDirectory(pluginDir);
            var files = Directory.GetFiles(pluginDir, "*.dll");
            foreach (var file in files)
            {
                try
                {
                    var types = Assembly.LoadFile(file).GetTypes().ToList()
                        .FindAll(m => m.GetInterface(typeof(IPlugin).FullName) != null && !m.IsAbstract)
                        .Select(s => (IPlugin)Activator.CreateInstance(s)).ToList();
                    _plugins.AddRange(types);
                }
                catch (Exception e)
                {
                }
            }
        }

        public new void Dispose()
        {
            _browser.Visible = false;
            base.Dispose();
            _browser?.Dispose();
            _browser = null;
        }

        private void ChromeWebBrowserUC_Load(object sender, EventArgs e)
        {
            _browser.Visible = true;
        }
        
        private void InitializeResFinder()
        {
            this.splitContainer1.Panel2Collapsed = true;
            this.textBoxResUrl.Text = "";
            buttonFindNextRes.PerformClick();
            this.textBoxResUrl.OnEnterPressed((c) => {
                buttonFindNextRes.PerformClick();
            });
        }

        private void InitializeBrowser()
        {
            _browser = new ChromiumWebBrowser(_url);
            _browser.TitleChanged += _browser_TitleChanged;
            _browser.LifeSpanHandler = _Handlerservice.GetLifeSpanHandler(_addTabAction);
            _browser.KeyboardHandler = CreateKeyboardHandler();
            _browser.MenuHandler = _Handlerservice.GetContextMenuHandler();
            _browser.DownloadHandler = _Handlerservice.GetDownloadHandler();
            _browser.RequestHandler = _Handlerservice.GetRequestHandler(myResReplaceRequestHandler_BeforeResourceLoad); // 替换资源
            _browser.AddressChanged += _browser_AddressChanged; // 地址变化
            _browser.LoadingStateChanged += _browser_LoadingStateChanged; // 加载变化
            _browser.DisplayHandler = _Handlerservice.GetDisplayHandler(); // 全屏捕获
            _browser.Visible = false;
            _Handlerservice.GetJsObjectRegister().Register(_browser, _plugins); // 注册js
            // 默认采用cookie隔离
            // https://blog.csdn.net/u010919083/article/details/78366906
            // 可以开小号
            if (CefSharpBrowserRepo.AllowMultiAccountCookie)
            {
                RequestContextSettings requestContextSettings = new RequestContextSettings();
                requestContextSettings.PersistSessionCookies = false;
                requestContextSettings.PersistUserPreferences = false;
                _browser.RequestContext = new RequestContext(requestContextSettings);
            }
            // url enter
            this.toolStripTextBoxUrl.OnEnterPressed((c) => {
                LoadUrl(c.Text);
            });

            this.panel1.Controls.Add(_browser);
            _browser.Dock = DockStyle.Fill;
        }

        private IKeyboardHandler CreateKeyboardHandler()
        {
            var handler = _Handlerservice.GetKeyboardHandler();
            handler.AddHotKey(Keys.F5,false,false,false, () => this.RunOnUIAsync(Reload));
            handler.AddHotKey(Keys.F12, false, false, false, () => this.RunOnUIAsync(_browser.ShowDevTools));
            handler.AddHotKey(Keys.T, true, false, false, () => this.RunOnUIAsync<string>(_addTabAction.Invoke, CefSharpBrowserRepo.HomeUrl));
            handler.AddHotKey(Keys.W, true, false, false, () => this.RunOnUIAsync(_removeTabAction.Invoke));

            return handler;
        }

        private void toolStripButtonNavTools_Click(object sender, EventArgs e)
        {
            ToolStripButton btn = sender as ToolStripButton;
            if (btn != null)
            {
                string type = btn.Tag?.ToString();
                switch (type)
                {
                    case "forward":
                        _browser.Forward();
                        break;
                    case "back":
                        _browser.Back();
                        break;
                    case "refresh":
                        Reload();
                        break;
                    case "home":
                        LoadUrl(CefSharpBrowserRepo.HomeUrl);
                        break;
                }
            }
        }

        #region CefSharpBrowser handler
        /// <summary>
        /// url地址变化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _browser_AddressChanged(object sender, AddressChangedEventArgs e)
        {
            this.RunOnUI(() =>
            {
                this.toolStripTextBoxUrl.Text = e.Address;
                LoadMatchedPlugins(e.Address);
            });
        }

        /// <summary>
        /// 网页标题变化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _browser_TitleChanged(object sender, TitleChangedEventArgs e)
        {
            this.RunOnUI(() => _changeTabTitle?.Invoke(this.Tag, e.Title));
        }

        /// <summary>
        /// 网页加载状态变化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _browser_LoadingStateChanged(object sender, LoadingStateChangedEventArgs e)
        {
            this.RunOnUIAsync(() =>
            {
                this.toolStripButtonForward.Enabled = e.CanGoForward;
                this.toolStripButtonBack.Enabled = e.CanGoBack;
                if (e.IsLoading)
                {
                    this.toolStripStatusLabelState.Text = "正在加载...";
                }
                else
                {
                    this.toolStripStatusLabelState.Text = "加载完成";
                }
            });
        } 
        #endregion

        #region 资源

        /// <summary>
        /// 资源加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void myResReplaceRequestHandler_BeforeResourceLoad(object sender, string requestUrl)
        {
            var url = requestUrl;
            Console.WriteLine("res:" + url);
            var handler = sender as IMyResReplaceRequestHandler;
            this.RunOnUIAsync(() =>
            {
                var count = this.dataGridView1.Rows.Count;
                var replaced = handler.GetReplaced(url);

                // 增加资源替换功能
                var replacedText = replaced == null ? "未替换" : replaced.ToString();
                this.dataGridView1.Rows.Add(count + 1, url, replacedText, "替换");

                // 插件资源初始化
                _plugins.ForEach(a => a.BeforeResourceLoad(url, count + 1));

                // 搜索模式
                if (!string.IsNullOrEmpty(this.textBoxResUrl.Text))
                {
                    // 不包含关键字
                    if (!url.ToLower().Contains(this.textBoxResUrl.Text))
                    {
                        // 隐藏
                        this.dataGridView1.Rows[this.dataGridView1.Rows.Count - 1].Visible = false;
                    }
                }
            });
        }

        /// <summary>
        /// 替换资源
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0 || e.ColumnIndex < 0)
                return;
            if (e.ColumnIndex == 3)
            {
                var buttonText = this.dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
                if (buttonText == "替换")
                {
                    var url = this.dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
                    //MessageBox.Show(url);
                    var handler = _browser.RequestHandler as IMyResReplaceRequestHandler;
                    var dialog = new OpenFileDialog();
                    dialog.Multiselect = false;
                    if (dialog.ShowDialog() == DialogResult.OK)
                    {
                        handler.AddFileReplaceRule(url, dialog.FileName);
                        this.dataGridView1.Rows[e.RowIndex].Cells[2].Value = dialog.FileName;
                    }
                }
            }
        }

        /// <summary>
        /// 搜索资源
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonFindNextRes_Click(object sender, EventArgs e)
        {
            /*
             * 1. 如果关键字为空或者空白，则全部显示
             * 2. 如果关键字不为空白，且不存在该关键字，则全部不显示
             * 3. 如果关键字不为空白，且存在该关键字，则存在为显示、不存在为不显示
             */
            var keyword = this.textBoxResUrl.Text;

            // 1.
            if (string.IsNullOrWhiteSpace(keyword))
            {
                for (int i = 0; i < this.dataGridView1.RowCount; i++)
                {
                    this.dataGridView1.Rows[i].Visible = true;
                }
                return;
            }
            // 拆分多个关键字
            var keywordList = keyword.Split("| ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            List<int> foundIndexes = new List<int>();
            for (int i = 0; i < this.dataGridView1.RowCount; i++)
            {
                var url = this.dataGridView1.Rows[i].Cells[1].Value.ToString();
                foreach (var item in keywordList)
                {
                    if (url.ToLower().Contains(item.ToLower()))
                    {
                        foundIndexes.Add(i);
                        break;
                    }
                }
            }
            if (foundIndexes.Count == 0)
            {
                // 2.
                for (int i = 0; i < this.dataGridView1.RowCount; i++)
                {
                    this.dataGridView1.Rows[i].Visible = false;
                }
            }
            else
            {
                // 3.
                for (int i = 0; i < this.dataGridView1.RowCount; i++)
                {
                    this.dataGridView1.Rows[i].Visible = foundIndexes.Contains(i);
                }
            }
        }

        /// <summary>
        /// 关闭底部资源栏
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripMenuItemCloseResBar_Click(object sender, EventArgs e)
        {
            RunCommand("show_res_bar");
        }

        private void contextMenuStripRes_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if(this.dataGridView1.SelectedCells.Count == 0)
            {
                e.Cancel = true;
                return;
            }
        }

        private void 复制请求地址ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<string> selectedUrls = new List<string>();
            for (int i = 0; i < this.dataGridView1.SelectedCells.Count; i++)
            {
                var cell = this.dataGridView1.SelectedCells[i];
                var row = this.dataGridView1.Rows[cell.RowIndex];
                if (!row.Visible)
                    continue;
                var url = this.dataGridView1.Rows[cell.RowIndex].Cells[1].Value.ToString();
                if(!selectedUrls.Contains(url))
                    selectedUrls.Add(url);
            }
            if (selectedUrls.Count == 0)
                return;
            Clipboard.SetText(string.Join("\r\n", selectedUrls));
        }

        private void 使用图片关键字ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // https://blog.csdn.net/m0_46665077/article/details/126374620
            textBoxResUrl.Text = ".bmp|.jpg|.jpeg|.jpe|.jxr|.png|.tif|.tiff|.avif|.xbm|.pjp|.svgz|.ico|.svg|.jfif|.webp|.pjpeg|.gif|.iff|.ilbm|.ppm|.pcx|.xcf|.xpm|.psd|.mng|.sai|.psp|.ufo";
        }

        private void 使用视频关键字ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBoxResUrl.Text = ".mp4|.3gp|.avi|.wmv|.mpeg|.mpg|.mov|.flv|.swf|.qsv|.kux|.mpg|.rm|.ram";
        }

        private void 使用音频关键字ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBoxResUrl.Text = ".mp3|.mp2|.mp1|.wav|.aif|.aiff|.au|.ra|.rm|.ram|.mid|.rmi";
        }

        private void 使用m3u8关键字ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBoxResUrl.Text = ".m3u8";
        }
        #endregion

        #region 加载已启用的插件
        private void LoadMatchedPlugins(string url)
        {
            this.toolStripDropDownButtonEnabledPlugins.DropDownItems.Clear();
            _plugins.ForEach(a=>a.AfterAddressChanged(url));
            this.toolStripDropDownButtonEnabledPlugins.Text = "已启用的插件(" + this.toolStripDropDownButtonEnabledPlugins.DropDownItems.Count + "个)";
        } 
        #endregion

        /// <summary>
        /// 重新加载页面时，调整界面数据
        /// </summary>
        /// <param name="url"></param>
        private void ChangeUIDataWhenLoad(string url)
        {
            this.dataGridView1.Rows.Clear();
            this.toolStripTextBoxUrl.Text = url;
        }
    }
}
