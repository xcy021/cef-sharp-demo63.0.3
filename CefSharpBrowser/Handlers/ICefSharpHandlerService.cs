﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CefSharp;
using CefSharpBrowser.Handlers.Cookie;
using CefSharpBrowser.Handlers.Js;
using CefSharpBrowser.Handlers.Keyboard;
using CefSharpBrowser.Handlers.Resource;

namespace CefSharpBrowser.Handlers
{
    public interface IHandlerservice
    {
        IContextMenuHandler GetContextMenuHandler();
        IMyKeyboardHandler GetKeyboardHandler();
        ILifeSpanHandler GetLifeSpanHandler(Action<string> addTabAction);
        IMyResReplaceRequestHandler GetRequestHandler(Action<object, string> beforeResourceLoad);
        IMyJsObjectRegister GetJsObjectRegister();
        IMyCookieManager GetCookieManager();
        IDownloadHandler GetDownloadHandler();
        IDisplayHandler GetDisplayHandler();
    }
}
