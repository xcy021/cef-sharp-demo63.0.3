﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CefSharp;

namespace CefSharpBrowser.Handlers.Cookie
{
    class MyCookieManagerProxy : IMyCookieManager
    {
        private ICookieManager _cookieManager;

        public MyCookieManagerProxy(ICookieManager cookieManager)
        {
            _cookieManager = cookieManager;
        }

        public void Dispose()
        {
            _cookieManager.Dispose();
        }

        public bool DeleteCookies(string url = null, string name = null, IDeleteCookiesCallback callback = null)
        {
            return _cookieManager.DeleteCookies(url, name, callback);
        }

        public bool SetCookie(string url, CefSharp.Cookie cookie, ISetCookieCallback callback = null)
        {
            return _cookieManager.SetCookie(url, cookie, callback);
        }

        public bool SetStoragePath(string path, bool persistSessionCookies, ICompletionCallback callback = null)
        {
            return _cookieManager.SetStoragePath(path, persistSessionCookies, callback);
        }

        public void SetSupportedSchemes(string[] schemes, ICompletionCallback callback = null)
        {
            _cookieManager.SetSupportedSchemes(schemes, callback);
        }

        public bool VisitAllCookies(ICookieVisitor visitor)
        {
            return _cookieManager.VisitAllCookies(visitor);
        }

        public bool VisitUrlCookies(string url, bool includeHttpOnly, ICookieVisitor visitor)
        {
            return _cookieManager.VisitUrlCookies(url, includeHttpOnly, visitor);
        }

        public bool FlushStore(ICompletionCallback callback)
        {
            return _cookieManager.FlushStore(callback);
        }

        public bool IsDisposed
        {
            get { return _cookieManager.IsDisposed; }
        }

        public void ClearCookie()
        {
            _cookieManager.DeleteCookies("", "");
        }

        public void AddCookie(string url, CefSharp.Cookie c)
        {
            _cookieManager.SetCookie(url, c);
        }
    }
}
