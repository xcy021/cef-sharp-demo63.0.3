﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CefSharp;

namespace CefSharpBrowser.Handlers.Cookie
{
    public interface IMyCookieManager : ICookieManager
    {
        void ClearCookie();
        void AddCookie(string url, CefSharp.Cookie c);
    }
}
