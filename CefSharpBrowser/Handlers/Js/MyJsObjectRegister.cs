﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using CefSharp;
using CefSharpBrowser.Plugins;

namespace CefSharpBrowser.Handlers.Js
{
    class MyJsObjectRegister : IMyJsObjectRegister
    {
        private List<IPlugin> plugins;
        public void Register(IWebBrowser webBrowser, List<IPlugin> plugins)
        {
            this.plugins = plugins;
            webBrowser.Register("myWin", new MyJsObject(), _browser_FrameLoadEnd);
        }

        private void _browser_FrameLoadEnd(object sender, FrameLoadEndEventArgs e)
        {
            IWebBrowser webBrowser = sender as IWebBrowser;
            plugins.ForEach(a=>a.BeforeFrameLoadEnd((code)=> webBrowser.GetBrowser().MainFrame.ExecuteJavaScriptAsync(code), webBrowser.Address));
        }
    }
}
