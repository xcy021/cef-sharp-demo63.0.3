﻿using System.Collections.Generic;
using CefSharp;
using CefSharpBrowser.Plugins;

namespace CefSharpBrowser.Handlers.Js
{
    public interface IMyJsObjectRegister
    {
        void Register(IWebBrowser webBrowser, List<IPlugin> plugins);
    }
}