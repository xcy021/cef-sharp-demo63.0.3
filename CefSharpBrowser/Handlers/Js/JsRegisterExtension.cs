﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CefSharp;

namespace CefSharp
{
    public static class JsRegisterExtension
    {
        public static void Register(this IWebBrowser browser, string jsObjName, object csharpObj, Action<object, FrameLoadEndEventArgs> browserFrameLoadEnd)
        {
            new MyClass().Register(browser, jsObjName, csharpObj, browserFrameLoadEnd);
        }

        class MyClass
        {
            private string jsObjName;
            private object csharpObj;
            private Action<object, FrameLoadEndEventArgs> browserFrameLoadEnd;
            public void Register(IWebBrowser webBrowser, string jsObjName, object csharpObj, Action<object, FrameLoadEndEventArgs> browserFrameLoadEnd)
            {
                this.jsObjName = jsObjName;
                this.csharpObj = csharpObj;
                this.browserFrameLoadEnd = browserFrameLoadEnd;
                webBrowser.FrameLoadEnd += _browser_FrameLoadEnd;
                RegisterCSharpObjectToWeb(webBrowser);
            }

            /// <summary>
            /// 将c#对象注册到网页中，从网页中可以直接访问该对象
            /// </summary>
            private void RegisterCSharpObjectToWeb(IWebBrowser webBrowser)
            {
                webBrowser.JavascriptObjectRepository.ResolveObject += (s, eve) =>
                {
                    var repo = eve.ObjectRepository;
                    if (eve.ObjectName == this.jsObjName)
                    {
                        if(repo.IsBound(this.jsObjName) == false)
                            repo.Register(this.jsObjName, this.csharpObj, isAsync: false, options: BindingOptions.DefaultBinder);
                    }
                };
            }

            private void _browser_FrameLoadEnd(object sender, FrameLoadEndEventArgs e)
            {
                Console.WriteLine($"FrameLoadEnd {e.Url}");
                IWebBrowser webBrowser = sender as IWebBrowser;
                if (webBrowser.IsBrowserInitialized)
                {
                    webBrowser.GetBrowser().MainFrame
                        .ExecuteJavaScriptAsync("CefSharp.BindObjectAsync('" + this.jsObjName + "');");

                    browserFrameLoadEnd?.Invoke(sender, e);
                }
            }
        }
    }
}
