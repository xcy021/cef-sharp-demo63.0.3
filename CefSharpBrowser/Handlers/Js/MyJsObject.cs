﻿using System.Windows.Forms;
using Newtonsoft.Json;

namespace CefSharpBrowser.Handlers.Js
{
    class MyJsObject
    {

        public void showMessage(string msg)
        {
            MessageBox.Show(msg);
        }

        public void testString(string msg)
        {
            MessageBox.Show(msg);
        }

        public void testObject(object msg)
        {
            MessageBox.Show(msg.ToString());
            MessageBox.Show(JsonConvert.SerializeObject(msg));
        }
    }
}
