﻿namespace CefSharpBrowser.Handlers.Initializer
{
    public interface IMyCefInitializer
    {
        /// <summary>
        /// 初始化cef设置
        /// </summary>
        void InitializeCefSettings();

        string UserAgent { get; set; }
    }
}