﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CefSharp;

namespace CefSharpBrowser.Handlers.Initializer
{
    class MyCefInitializer : IMyCefInitializer
    {
        public string UserAgent { get; set; }

        public MyCefInitializer()
        {
            this.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 QIHU 360EE/13.5.2015.0";
        }

        /// <summary>
        /// 初始化cef设置
        /// </summary>
        public void InitializeCefSettings()
        {
            var settings = new CefSettings();
            CefSharpSettings.WcfEnabled = true;
            // 设置缓存目录，下次账号自动登录
            var appDir = System.AppDomain.CurrentDomain.BaseDirectory;
            //settings.CachePath = System.IO.Path.Combine(appDir, "cef_cache");
            //settings.RemoteDebuggingPort = 8088;
            settings.Locale = "zh-CN"; // 右键菜单默认中文
            settings.AcceptLanguageList = "zh-CN"; // 网站默认中文
            settings.IgnoreCertificateErrors = true;
            settings.LogFile = System.IO.Path.Combine(appDir, "cef_log/" + Guid.NewGuid().ToString() + ".txt");
            //settings.CefCommandLineArgs.Add("ppapi-flash-path", System.AppDomain.CurrentDomain.BaseDirectory + "pepflashplayer.dll"); //指定flash的版本，不使用系统安装的flash版本
            //settings.CefCommandLineArgs.Add("ppapi-flash-version", "29.0.0.171");
            //settings.CefCommandLineArgs.Add("enable-media-stream", "1");
            //settings.CefCommandLineArgs.Add("no-proxy-server", "1");
            //settings.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.79 Safari/537.36";

            //settings.LogSeverity = LogSeverity.Verbose;
            settings.UserAgent = this.UserAgent;
            Cef.EnableHighDPISupport(); // 解决网页里的元素点击位置与界面显示的问题不一致，鼠标必须向上偏移一些才能点击到元素的问题
            if (!Cef.Initialize(settings))
                throw new Exception("Unable to Initialize Cef");
        }
    }
}
