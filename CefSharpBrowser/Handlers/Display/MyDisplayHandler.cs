﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using CefSharp;
using CefSharp.Structs;
using CefSharp.WinForms;

namespace CefSharpBrowser.Handlers.Display
{
    // https://github.com/cefsharp/CefSharp/blob/842429a76e03bef75e6a47543f84aa6e513bbf74/CefSharp.Wpf.Example/Handlers/DisplayHandler.cs
    // js调用全屏和取消全屏
    // document.documentElement.webkitRequestFullScreen();
    // document.webkitCancelFullScreen();
    // 注意：以上两行js不能通过_browser.GetBrowser().MainFrame.ExecuteJavaScriptAsync();
    //      否则会提示Failed to execute ‘requestFullscreen’ on ‘Element’: API can only be initiated by a user gesture.
    //      https://www.cnblogs.com/joyco773/p/9792170.html
    public class MyDisplayHandler : DefaultDisplayHandler
    {
        private Control parent;
        private Form fullScreenWindow;

        protected override void OnFullscreenModeChange(IWebBrowser chromiumWebBrowser, IBrowser CefSharpBrowser, bool fullscreen)
        {
            var webBrowser = (ChromiumWebBrowser)chromiumWebBrowser;

            webBrowser.BeginInvoke((Action)(() =>
            {
                if (fullscreen)
                {
                    //In this example the parent is a Grid, if your parent is a different type
                    //of control then update this code accordingly.
                    parent = webBrowser.Parent;

                    //NOTE: If the ChromiumWebBrowser instance doesn't have a direct reference to
                    //the DataContext in this case the CefSharpBrowserTabViewModel then your bindings won't
                    //be updated/might cause issues like the CefSharpBrowser reloads the Url when exiting
                    //fullscreen.
                    parent.Controls.Remove(webBrowser);

                    fullScreenWindow = new Form
                    {
                        FormBorderStyle = FormBorderStyle.None,
                        WindowState = FormWindowState.Maximized,
                    };
                    fullScreenWindow.Controls.Add(webBrowser);

                    fullScreenWindow.ShowDialog();
                }
                else
                {
                    fullScreenWindow.Controls.Clear();

                    parent.Controls.Add(webBrowser);

                    fullScreenWindow.Close();
                    fullScreenWindow = null;
                    parent = null;
                }
            }));
        }
    }
}
