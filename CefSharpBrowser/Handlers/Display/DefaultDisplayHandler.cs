﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CefSharp;
using CefSharp.Structs;

namespace CefSharpBrowser.Handlers.Display
{
    /// <summary>
    /// Handle events related to CefSharpBrowser display state.
    /// </summary>
    public class DefaultDisplayHandler : IDisplayHandler
    {

        /// <inheritdoc/>
        void IDisplayHandler.OnAddressChanged(IWebBrowser chromiumWebBrowser, AddressChangedEventArgs addressChangedArgs)
        {
            OnAddressChanged(chromiumWebBrowser, addressChangedArgs);
        }

        /// <summary>
        /// Called when a frame's address has changed. 
        /// </summary>
        /// <param name="chromiumWebBrowser">the ChromiumWebBrowser control</param>
        /// <param name="addressChangedArgs">args</param>
        protected virtual void OnAddressChanged(IWebBrowser chromiumWebBrowser, AddressChangedEventArgs addressChangedArgs)
        {

        }

        /// <inheritdoc/>
        bool IDisplayHandler.OnAutoResize(IWebBrowser chromiumWebBrowser, IBrowser CefSharpBrowser, Size newSize)
        {
            return OnAutoResize(chromiumWebBrowser, CefSharpBrowser, newSize);
        }

        /// <summary>
        /// Called when auto-resize is enabled via IBrowserHost.SetAutoResizeEnabled and the contents have auto-resized.
        /// </summary>
        /// <param name="chromiumWebBrowser">the ChromiumWebBrowser control</param>
        /// <param name="browser">the CefSharpBrowser object</param>
        /// <param name="newSize">will be the desired size in view coordinates</param>
        /// <returns>Return true if the resize was handled or false for default handling. </returns>
        protected virtual bool OnAutoResize(IWebBrowser chromiumWebBrowser, IBrowser CefSharpBrowser, Size newSize)
        {
            return false;
        }

        /// <inheritdoc/>
        void IDisplayHandler.OnTitleChanged(IWebBrowser chromiumWebBrowser, TitleChangedEventArgs titleChangedArgs)
        {
            OnTitleChanged(chromiumWebBrowser, titleChangedArgs);
        }

        /// <summary>
        /// Called when the page title changes.
        /// </summary>
        /// <param name="chromiumWebBrowser">the ChromiumWebBrowser control</param>
        /// <param name="titleChangedArgs">args</param>
        protected virtual void OnTitleChanged(IWebBrowser chromiumWebBrowser, TitleChangedEventArgs titleChangedArgs)
        {

        }

        /// <inheritdoc/>
        void IDisplayHandler.OnFaviconUrlChange(IWebBrowser chromiumWebBrowser, IBrowser CefSharpBrowser, IList<string> urls)
        {
            OnFaviconUrlChange(chromiumWebBrowser, CefSharpBrowser, urls);
        }

        /// <summary>
        /// Called when the page icon changes.
        /// </summary>
        /// <param name="chromiumWebBrowser">the ChromiumWebBrowser control</param>
        /// <param name="browser">the CefSharpBrowser object</param>
        /// <param name="urls">list of urls where the favicons can be downloaded</param>
        protected virtual void OnFaviconUrlChange(IWebBrowser chromiumWebBrowser, IBrowser CefSharpBrowser, IList<string> urls)
        {

        }

        /// <inheritdoc/>
        void IDisplayHandler.OnFullscreenModeChange(IWebBrowser chromiumWebBrowser, IBrowser CefSharpBrowser, bool fullscreen)
        {
            OnFullscreenModeChange(chromiumWebBrowser, CefSharpBrowser, fullscreen);
        }

        /// <summary>
        /// Called when web content in the page has toggled fullscreen mode. The client is
        /// responsible for resizing the CefSharpBrowser if desired.
        /// </summary>
        /// <param name="chromiumWebBrowser">The ChromiumWebBrowser control</param>
        /// <param name="browser">the CefSharpBrowser object</param>
        /// <param name="fullscreen">If true the content will automatically be sized to fill the CefSharpBrowser content area.
        /// If false the content will automatically return to its original size and position.</param>
        protected virtual void OnFullscreenModeChange(IWebBrowser chromiumWebBrowser, IBrowser CefSharpBrowser, bool fullscreen)
        {

        }

        /// <inheritdoc/>
        bool IDisplayHandler.OnTooltipChanged(IWebBrowser chromiumWebBrowser, ref string text)
        {
            return OnTooltipChanged(chromiumWebBrowser, ref text);
        }

        /// <summary>
        /// Called when the CefSharpBrowser is about to display a tooltip. text contains the
        /// text that will be displayed in the tooltip. You can optionally modify text
        /// and then return false to allow the CefSharpBrowser to display the tooltip.
        /// When window rendering is disabled the application is responsible for
        /// drawing tooltips and the return value is ignored.
        /// </summary>
        /// <param name="chromiumWebBrowser">The ChromiumWebBrowser control</param>
        /// <param name="text">the text that will be displayed in the tooltip</param>
        /// <returns>To handle the display of the tooltip yourself return true otherwise return false
        /// to allow the CefSharpBrowser to display the tooltip.</returns>
        /// <remarks>Only called when using Off-screen rendering (WPF and OffScreen)</remarks>
        protected virtual bool OnTooltipChanged(IWebBrowser chromiumWebBrowser, ref string text)
        {
            return false;
        }

        /// <inheritdoc/>
        void IDisplayHandler.OnStatusMessage(IWebBrowser chromiumWebBrowser, StatusMessageEventArgs statusMessageArgs)
        {

        }

        /// <summary>
        /// Called when the CefSharpBrowser receives a status message.
        /// </summary>
        /// <param name="chromiumWebBrowser">The <see cref="IWebBrowser"/> control this popup is related to.</param>
        /// <param name="statusMessageArgs">args</param>
        protected virtual void OnStatusMessage(IWebBrowser chromiumWebBrowser, StatusMessageEventArgs statusMessageArgs)
        {

        }

        /// <inheritdoc/>
        bool IDisplayHandler.OnConsoleMessage(IWebBrowser chromiumWebBrowser, ConsoleMessageEventArgs consoleMessageArgs)
        {
            return OnConsoleMessage(chromiumWebBrowser, consoleMessageArgs);
        }

        /// <summary>
        /// Called to display a console message. 
        /// </summary>
        /// <param name="chromiumWebBrowser">The ChromiumWebBrowser control</param>
        /// <param name="consoleMessageArgs">args</param>
        /// <returns>Return true to stop the message from being output to the console.</returns>
        protected virtual bool OnConsoleMessage(IWebBrowser chromiumWebBrowser, ConsoleMessageEventArgs consoleMessageArgs)
        {
            return false;
        }
    }
}
