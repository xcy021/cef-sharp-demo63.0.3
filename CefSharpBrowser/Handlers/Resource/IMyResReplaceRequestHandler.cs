﻿using System;
using CefSharp;

namespace CefSharpBrowser.Handlers.Resource
{
    public interface IMyResReplaceRequestHandler : IRequestHandler
    {
        object GetReplaced(string urlPattern);
        void AddFileReplaceRule(string urlPattern, string localFile);
        void SaveReplaceRule();
        void ReadReplaceRule();
        event Action<object, string> BeforeResourceLoad;
    }
}