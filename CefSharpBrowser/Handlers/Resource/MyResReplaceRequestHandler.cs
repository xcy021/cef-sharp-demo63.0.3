﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using CefSharp;
using CefSharp.Handler;

namespace CefSharpBrowser.Handlers.Resource
{
    class MyResReplaceRequestHandler : DefaultRequestHandler, IMyResReplaceRequestHandler
    {
        List<string> urlPatterns = new List<string>();
        List<object> replaces = new List<object>();

        public event Action<object, string> BeforeResourceLoad; // MyResReplaceRequestHandler对象、RequestUrl

        string resReplaceDir;
        string resReplaceFile;

        public MyResReplaceRequestHandler()
        {
            resReplaceDir = CefSharpBrowserRepo.GetPath(AppPath.ResReplace);
            if (!Directory.Exists(resReplaceDir))
                Directory.CreateDirectory(resReplaceDir);

            resReplaceFile = Path.Combine(resReplaceDir, "Data.txt");
            if (!File.Exists(resReplaceFile))
                File.Create(resReplaceFile).Close();
        }

        public override bool OnBeforeBrowse(IWebBrowser browserControl, IBrowser browser, IFrame frame, IRequest request, bool isRedirect)
        {
            return base.OnBeforeBrowse(browserControl, browser, frame, request, isRedirect);
        }

        public override IResponseFilter GetResourceResponseFilter(IWebBrowser CefSharpBrowserControl, IBrowser CefSharpBrowser, IFrame frame, IRequest request, IResponse response)
        {
            var replace = GetReplaced(request.Url);
            if(replace == null)
            {
                return null;
            }
            else
            {
                if (replace is string)
                {
                    return new MyResReplaceResponseFilter(replace as string);
                }
                else if (replace is byte[])
                {
                    return new MyResReplaceResponseFilter(replace as byte[]);
                }
                else if (replace is Stream)
                {
                    return new MyResReplaceResponseFilter(replace as Stream);
                }
                return null;
            }
        }

        public override CefReturnValue OnBeforeResourceLoad(IWebBrowser browserControl, IBrowser browser, IFrame frame, IRequest request, IRequestCallback callback)
        {
            if(BeforeResourceLoad != null)
            {
                BeforeResourceLoad(this, request.Url);
            }
            return base.OnBeforeResourceLoad(browserControl, browser, frame, request, callback);
        }

        public object GetReplaced(string urlPattern)
        {
            if (string.IsNullOrWhiteSpace(urlPattern))
                return null;
            for (int i = 0; i < urlPatterns.Count; i++)
            {
                var item = urlPatterns[i];
                if (Regex.IsMatch(urlPattern, item) || urlPattern == item || urlPattern.Contains(item))
                {
                    var replace = replaces[i];
                    return replace;
                }
            }
            return null;
        }

        public void AddFileReplaceRule(string urlPattern, string localFile)
        {
            AddReplaceRule(urlPattern, localFile);
        }

        public void AddBinaryReplaceRule(string urlPattern, Stream stream)
        {
            AddReplaceRule(urlPattern, stream);
        }

        public void AddBinaryReplaceRule(string urlPattern, byte[] bytes)
        {
            AddReplaceRule(urlPattern, bytes);
        }

        public void SaveReplaceRule()
        {
            var lines = new string[urlPatterns.Count];
            for (int i = 0; i < urlPatterns.Count; i++)
            {
                if(replaces[i] is string)
                    lines[i] = urlPatterns[i] + "," + replaces[i];
            }
            System.IO.File.WriteAllLines(resReplaceFile, lines);
        }

        public void ReadReplaceRule()
        {
            var lines = File.ReadAllLines(resReplaceFile);
            foreach (var item in lines)
            {
                var values = item.Split(',');
                var file = values[1];
                if (!File.Exists(file))
                {
                    file = Path.Combine(resReplaceDir, Path.GetFileName(file));
                    if (!File.Exists(file))
                        continue;
                }
                AddReplaceRule(values[0], file);
            }
        }

        private void AddReplaceRule(string urlPattern, object replace)
        {
            var foundIndex = urlPatterns.IndexOf(urlPattern);
            if(foundIndex == -1)
            {
                urlPatterns.Add(urlPattern);
                replaces.Add(replace);
            }
            else
            {
                replaces[foundIndex] = replace;
            }
        }
    }
}
