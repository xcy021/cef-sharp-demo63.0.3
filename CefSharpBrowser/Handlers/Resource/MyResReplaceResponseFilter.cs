﻿using System.IO;
using CefSharp;

namespace CefSharpBrowser.Handlers.Resource
{

    class MyResReplaceResponseFilter : IResponseFilter
    {
        private Stream _stream;

        public MyResReplaceResponseFilter(string localFile)
        {
            _stream = System.IO.File.OpenRead(localFile);
            _stream.Seek(0, SeekOrigin.Begin);
        }

        public MyResReplaceResponseFilter(byte[] bytes)
        {
            _stream = new MemoryStream(bytes);
            _stream.Seek(0, SeekOrigin.Begin);
        }

        public MyResReplaceResponseFilter(Stream stream)
        {
            _stream = stream;
            _stream.Seek(0, SeekOrigin.Begin);
        }

        public void Dispose()
        {
            _stream.Close();
            _stream.Dispose();
            _stream = null;
        }

        public FilterStatus Filter(Stream dataIn, out long dataInRead, Stream dataOut, out long dataOutWritten)
        {
            dataInRead = dataIn == null ? 0 : dataIn.Length;
            dataOutWritten = 0;

            byte[] buffer = new byte[1024];
            int size = _stream.Read(buffer, 0, buffer.Length);
            if (size == 0)
                return FilterStatus.Done;
            dataOut.Write(buffer, 0, size);
            dataOutWritten = size;
            return FilterStatus.NeedMoreData;
        }

        public bool InitFilter()
        {
            return true;
        }
    }
}
