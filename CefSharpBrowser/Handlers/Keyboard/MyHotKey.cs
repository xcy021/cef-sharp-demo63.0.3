﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CefSharpBrowser.Handlers.Keyboard
{
    class MyHotKey
    {
        public Keys KeyCode;
        public bool Control;
        public bool Alt;
        public bool Shift;

        public Action KeyPressAction;
    }
}
