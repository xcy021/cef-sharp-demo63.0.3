﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharp;

namespace CefSharpBrowser.Handlers.Keyboard
{
    public interface IMyKeyboardHandler : IKeyboardHandler
    {
        void AddHotKey(Keys keyCode, bool ctrl, bool alt, bool shift, Action keyPressAction);
    }
}
