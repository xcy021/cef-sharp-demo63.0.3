﻿using System;
using System.Collections.Concurrent;
using System.Windows.Forms;
using CefSharp;

namespace CefSharpBrowser.Handlers.Keyboard
{
    class MyKeyboardHandler : IMyKeyboardHandler
    {
        private ConcurrentBag<MyHotKey> myHotKeys = new ConcurrentBag<MyHotKey>();

        public void AddHotKey(Keys keyCode, bool ctrl, bool alt, bool shift, Action keyPressAction)
        {
            var myHotKey = new MyHotKey();
            myHotKey.KeyCode = keyCode;
            myHotKey.Control = ctrl;
            myHotKey.Alt = alt;
            myHotKey.Shift = shift;
            myHotKey.KeyPressAction = keyPressAction;
            myHotKeys.Add(myHotKey);
        }

        public bool OnKeyEvent(IWebBrowser CefSharpBrowserControl, IBrowser CefSharpBrowser, KeyType type, int windowsKeyCode, int nativeKeyCode, CefEventFlags modifiers, bool isSystemKey)
        {
            return true;
        }

        public bool OnPreKeyEvent(IWebBrowser CefSharpBrowserControl, IBrowser CefSharpBrowser, KeyType type, int windowsKeyCode, int nativeKeyCode, CefEventFlags modifiers, bool isSystemKey, ref bool isKeyboardShortcut)
        {
			if (type == KeyType.RawKeyDown)
            {
                int mod = ((int)modifiers);
                bool ctrlDown = IsBitmaskOn(mod, (int)CefEventFlags.ControlDown);
                bool shiftDown = IsBitmaskOn(mod, (int)CefEventFlags.ShiftDown);
                bool altDown = IsBitmaskOn(mod, (int)CefEventFlags.AltDown);

                // per registered hotkey
                foreach (var myHotKey in myHotKeys)
                {
                    if ((int)myHotKey.KeyCode == windowsKeyCode)
                    {
                        if (myHotKey.Control == ctrlDown && myHotKey.Shift == shiftDown && myHotKey.Alt == altDown)
                        {
                            myHotKey.KeyPressAction();
                        }
                    }
                }

                Console.WriteLine("keycode:{0},ctrl:{1},alt:{2},shift:{3}", (Keys) windowsKeyCode, ctrlDown, altDown,
                    shiftDown);
            }

			return false;
        }

        private bool IsBitmaskOn(int num, int bitmask)
        {
            return (num & bitmask) != 0;
        }
    }
}
