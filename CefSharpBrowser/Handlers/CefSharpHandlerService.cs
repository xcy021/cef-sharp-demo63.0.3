﻿using CefSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CefSharpBrowser.Handlers.ContextMenu;
using CefSharpBrowser.Handlers.Keyboard;
using CefSharpBrowser.Handlers.PagePopup;
using CefSharpBrowser.Handlers.Resource;
using System.IO;
using CefSharpBrowser.Handlers.Cookie;
using CefSharpBrowser.Handlers.Display;
using CefSharpBrowser.Handlers.Download;
using CefSharpBrowser.Handlers.Js;

namespace CefSharpBrowser.Handlers
{
    class Handlerservice : IHandlerservice
    {
        private readonly IContextMenuHandler _myDisableContextMenuHandler = new MyDisableContextMenuHandler();
        private readonly IMyJsObjectRegister _getJsObjectRegister = new MyJsObjectRegister();
        private readonly IMyKeyboardHandler _getKeyboardHandler = new MyKeyboardHandler();
        private IMyLifeSpanHandler _myDisablePopupLifeSpanHandler = null;
        private IMyResReplaceRequestHandler _myResReplaceRequestHandler;
        private IMyCookieManager _myCookieManagerProxy;
        private readonly IDownloadHandler _getDownloadHandler = new MyDownloadHandler();
        private readonly IDisplayHandler _displayHandler = new MyDisplayHandler();

        public IContextMenuHandler GetContextMenuHandler()
        {
            return _myDisableContextMenuHandler;
        }

        public IMyJsObjectRegister GetJsObjectRegister()
        {
            return _getJsObjectRegister;
        }

        public IMyCookieManager GetCookieManager()
        {
            if (_myCookieManagerProxy == null)
            {
                _myCookieManagerProxy = new MyCookieManagerProxy(Cef.GetGlobalCookieManager());
            }
            return _myCookieManagerProxy;
        }

        public IDownloadHandler GetDownloadHandler()
        {
            return _getDownloadHandler;
        }

        public IMyKeyboardHandler GetKeyboardHandler()
        {
            return _getKeyboardHandler;
        }

        public ILifeSpanHandler GetLifeSpanHandler(Action<string> addTabAction)
        {
            if (_myDisablePopupLifeSpanHandler == null)
            {
                _myDisablePopupLifeSpanHandler = new MyDisablePopupLifeSpanHandler();
                _myDisablePopupLifeSpanHandler.InitializeAddTabAction(addTabAction);
            }
            return _myDisablePopupLifeSpanHandler;
        }

        public IMyResReplaceRequestHandler GetRequestHandler(Action<object, string> beforeResourceLoad)
        {
            if (_myResReplaceRequestHandler == null)
            {
                _myResReplaceRequestHandler = new MyResReplaceRequestHandler();
                _myResReplaceRequestHandler.BeforeResourceLoad += beforeResourceLoad;
                _myResReplaceRequestHandler.ReadReplaceRule();
                var replace = _myResReplaceRequestHandler.GetReplaced("https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png");
                if (replace == null)
                {
                    // 百度图标 改为 搜狗图标
                    _myResReplaceRequestHandler.AddFileReplaceRule("https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png",
                        Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "logo_880x280_sogou.png"));
                    //handler.AddFileReplaceRule("img/PCtm", @"D:\logo_sogou.png");
                    //handler.AddBinaryReplaceRule("img/PCtm", new System.IO.MemoryStream(Properties.Resources.logo_880x280_sogou));
                }
            }

            return _myResReplaceRequestHandler;
        }

        public IDisplayHandler GetDisplayHandler()
        {
            return _displayHandler;
        }
    }
}
