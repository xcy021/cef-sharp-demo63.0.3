﻿using CefSharp;

namespace CefSharpBrowser.Handlers.ContextMenu
{
    class MyDisableContextMenuHandler : IContextMenuHandler
    {
        public void OnBeforeContextMenu(IWebBrowser CefSharpBrowserControl, IBrowser CefSharpBrowser, IFrame frame, IContextMenuParams parameters, IMenuModel model)
        {
        }

        public bool OnContextMenuCommand(IWebBrowser CefSharpBrowserControl, IBrowser CefSharpBrowser, IFrame frame, IContextMenuParams parameters, CefMenuCommand commandId, CefEventFlags eventFlags)
        {
            // 查看网页源码
            return false;
        }

        public void OnContextMenuDismissed(IWebBrowser CefSharpBrowserControl, IBrowser CefSharpBrowser, IFrame frame)
        {
        }

        public bool RunContextMenu(IWebBrowser CefSharpBrowserControl, IBrowser CefSharpBrowser, IFrame frame, IContextMenuParams parameters, IMenuModel model, IRunContextMenuCallback callback)
        {
            // 网页右键菜单
            return false;
        }
    }
}
