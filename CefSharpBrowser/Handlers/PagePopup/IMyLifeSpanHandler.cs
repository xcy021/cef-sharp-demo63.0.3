﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CefSharp;

namespace CefSharpBrowser.Handlers.PagePopup
{
    interface IMyLifeSpanHandler : ILifeSpanHandler
    {
        void InitializeAddTabAction(Action<string> addTabAction);
    }
}
