﻿using System;
using CefSharp;

namespace CefSharpBrowser.Handlers.PagePopup
{
    class MyDisablePopupLifeSpanHandler : IMyLifeSpanHandler
    {
        private Action<string> _addTabAction;

        public MyDisablePopupLifeSpanHandler()
        {
        }

        public void InitializeAddTabAction(Action<string> addTabAction)
        {
            _addTabAction = addTabAction;
        }

        public bool DoClose(IWebBrowser CefSharpBrowserControl, IBrowser CefSharpBrowser)
        {
            //  For default behaviour return false
            return false;
        }

        public void OnAfterCreated(IWebBrowser CefSharpBrowserControl, IBrowser CefSharpBrowser)
        {
        }

        public void OnBeforeClose(IWebBrowser CefSharpBrowserControl, IBrowser CefSharpBrowser)
        {
        }

        public bool OnBeforePopup(IWebBrowser CefSharpBrowserControl, IBrowser CefSharpBrowser, IFrame frame, string targetUrl, string targetFrameName, WindowOpenDisposition targetDisposition, bool userGesture, IPopupFeatures popupFeatures, IWindowInfo windowInfo, IBrowserSettings CefSharpBrowserSettings, ref bool noJavascriptAccess, out IWebBrowser newBrowser)
        {
            // To cancel creation of the popup window return true otherwise return false.
            // 不弹出新窗口
            //newBrowser = null;
            //browserControl.Load(targetUrl);
            //return true;

            // 弹出新窗口
            //newBrowser = null;
            //return false;

            // 在tab页中显示新页面
            newBrowser = null;
            if (_addTabAction != null)
            {
                _addTabAction(targetUrl); // 在新tab页中加载
            }
            else
            {
                CefSharpBrowserControl.Load(targetUrl); // 在当前页接续加载
            }
            return true;
        }
    }
}
