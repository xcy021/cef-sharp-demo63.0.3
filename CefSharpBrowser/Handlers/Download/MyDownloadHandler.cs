﻿using CefSharp;

namespace CefSharpBrowser.Handlers.Download
{
    class MyDownloadHandler : IDownloadHandler
    {
        public void OnBeforeDownload(IBrowser CefSharpBrowser, DownloadItem downloadItem, IBeforeDownloadCallback callback)
        {
            if (!callback.IsDisposed)
            {
                using (callback)
                {
                    callback.Continue(downloadItem.SuggestedFileName, showDialog: true);
                }
            }
        }

        public void OnDownloadUpdated(IBrowser CefSharpBrowser, DownloadItem downloadItem, IDownloadItemCallback callback)
        {
            
        }
    }
}
