﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using CefSharpBrowser;
using CefSharpBrowser.Plugins;

namespace Plugin.AnnieDownloader
{
    class AnnieDownloader : PluginAbstract
    {
        public override string PluginName => "使用annie下载";
        public override void AfterAddressChanged(string address)
        {
            if (IsUrlMatch(address))
            {
                this.afterFormLoadArgs.BrowserImp.AddPluginItem(this.PluginName, (sender, args) =>
                {
                    OnClick(address);
                });
            }
        }

        private bool IsUrlMatch(string url)
        {
            /*
             * https://v.qq.com/x/cover/05prmmdf1ppgdaq.html      .*://v.qq.com/x/cover.*
             * https://www.bilibili.com/bangumi/play/ss32999      .*://www.bilibili.com/bangumi/play/.*
             * https://www.iqiyi.com/v_2f4dldorhpk.html?vfrm=pcw_home&vfrmblk=B&vfrmrst=fcs_0_p11  .*://www.iqiyi.com/v_.*
             * https://www.bilibili.com/video/BV1zo4y1k7Ho?from=search&seid=11056804498958733958
             *
             */

            var patterns = new string[]
            {
                ".*://v.qq.com/x/cover.*",
                ".*://v.qq.com/x/page.*",
                ".*://.*.bilibili.com/bangumi/play/.*",
                ".*://.*.bilibili.com/listplay/.*",
                ".*://.*.bilibili.com/albumplay/.*",
                ".*://.*.bilibili.com/video/.*",
                ".*://.*.iqiyi.com/v_.*",
                ".*://.*.iqiyi.com/w_.*",
                ".*://.*.iqiyi.com/a_.*",
            };
            foreach (var pattern in patterns)
            {
                if (Regex.IsMatch(url, pattern))
                    return true;
            }

            return false;
        }

        private void OnClick(string url)
        {
            var downloadDir = CefSharpBrowserRepo.GetPath(AppPath.Download);
            // annie -o "D:\projects\Other\cef-sharp-demo63.0.3\output\Debug\tools\download" https://www.bilibili.com/video/BV1s54y1r7DX?spm_id_from=333.851.b_62696c695f7265706f72745f646f7567
            var annieArgs = "-o \"" + downloadDir + "\" " + url;
            //Process p = new Process();
            //p.StartInfo.FileName = "cmd.exe";
            //p.StartInfo.WorkingDirectory = annieDir;
            //p.StartInfo.RedirectStandardInput = true;
            //p.StartInfo.UseShellExecute = false;
            //p.Start();
            //p.StandardInput.Write(anniePath);
            //p.StandardInput.WriteLine(" " + url);

            Process.Start(new ProcessStartInfo()
            {
                FileName = GetAnnieFile(),
                Arguments = annieArgs,
                WorkingDirectory = GetAnnieDir()
            });
        }

        private string GetAnnieDir()
        {
            return Path.Combine(CefSharpBrowserRepo.GetPath(AppPath.Tools), @"annie\");
        }

        private string GetAnnieFile()
        {
            return Path.Combine(CefSharpBrowserRepo.GetPath(AppPath.Tools), @"annie\annie.exe");
        }
    }
}
