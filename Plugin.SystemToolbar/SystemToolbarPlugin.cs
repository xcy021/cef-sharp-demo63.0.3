﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharpBrowser;
using CefSharpBrowser.Plugins;

namespace Plugin.SystemToolbar
{
    public class SystemToolbarPlugin : PluginAbstract
    {
        public override string PluginName { get; } = "系统工具";
        
        public override void AfterFormLoad(AfterFormLoadArgs args)
        {
            base.AfterFormLoad(args);
            InitializeSystemToolbar();
            afterFormLoadArgs.BrowserImp.RegisterCommand("show_res_bar", () => RunSystemToolbar("show_res_bar"));
        }

        public override void AfterAddressChanged(string address)
        {
        }

        private ToolStripDropDownButton dropdown;
        private void InitializeSystemToolbar()
        {
            Dictionary<string, string> toolbars = new Dictionary<string, string>()
            {
                {"dev_tool", "开发者工具"},
                {"clear_cookie", "清除cookie"},
                {"invoke_js", "调用js"},
                {"show_res_bar", "显示资源栏"},
                {"open_video_download_dir", "打开下载目录"},
                {"load_cookie", "加载cookie"},
                {"open_allow_other_account_window", "打开小号窗口"},
            };
            dropdown = afterFormLoadArgs.BrowserImp.CreateAndInsertDropDownButton("系统功能");
            foreach (var kv in toolbars)
            {
                var toolbar = new System.Windows.Forms.ToolStripMenuItem();
                toolbar.Size = new System.Drawing.Size(224, 26);
                toolbar.Tag = kv.Key;
                toolbar.Text = kv.Value;
                toolbar.Click += this.ToolStripMenuItemSystemTool_Click;

                dropdown.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { toolbar });
            }
        }

        private void ToolStripMenuItemSystemTool_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem mi = sender as ToolStripMenuItem;
            if (mi != null)
            {
                string type = mi.Tag?.ToString();
                switch (type)
                {
                    case "dev_tool":
                        afterFormLoadArgs.BrowserImp.ShowDevTools();
                        break;
                    case "invoke_js":
                        afterFormLoadArgs.BrowserImp.ExecuteJavaScriptAsync("myWin.showMessage('hello winform');");
                        break;
                    case "clear_cookie":
                        afterFormLoadArgs.BrowserImp.ClearCookie();
                        break;
                    case "show_res_bar":
                        mi.Checked = !mi.Checked;
                        var isChecked = mi.Checked;
                        afterFormLoadArgs.BrowserImp.ShowResPanel(isChecked);
                        break;
                    case "open_video_download_dir":
                        var videoDownloadDir = CefSharpBrowserRepo.GetPath(AppPath.Download);
                        try
                        {
                            Process.Start("explorer.exe", videoDownloadDir);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                        }
                        break;
                    case "load_cookie":
                        LoadCookie();
                        break;
                    case "open_allow_other_account_window":
                        {
                            var frmSetUserAgent = new FrmSetUserAgent();
                            if (frmSetUserAgent.ShowDialog() != DialogResult.OK)
                            {
                                return;
                            }
                            var userAgent = frmSetUserAgent.UserAgent;
                            try
                            {
                                Process.Start(Application.ExecutablePath, 
                                    $"/AllowMultiAccountCookie:true \"/SetUserAgent:{userAgent}\"");
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex);
                            }
                        }
                        break;
                }
            }
        }

        private void RunSystemToolbar(string toolbarTag)
        {
            for (int i = 0; i < dropdown.DropDownItems.Count; i++)
            {
                ToolStripItem item = dropdown.DropDownItems[i];
                if (item.Tag?.ToString() == toolbarTag)
                {
                    item.PerformClick();
                    break;
                }
            }
        }

        private void LoadCookie()
        {
            var mainForm = afterFormLoadArgs.BrowserImp.FindForm();
            if (CefSharpBrowserRepo.AllowMultiAccountCookie)
            {
                MessageBox.Show(mainForm, "小号模式不支持cookie加载！", "提示", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return;
            }
            var frm = new Plugin.SystemToolbar.FrmSetCookie();
            frm.Icon = mainForm.Icon;
            if (frm.ShowDialog() == DialogResult.OK)
            {
                var cookie = frm.Cookie;
                var url = afterFormLoadArgs.BrowserImp.GetAddress();
                if (url.Contains("://"))
                {
                    int index1 = url.IndexOf("://") + "://".Length;
                    int index2 = url.IndexOf("/", index1);
                    if (index2 == -1)
                        index2 = url.Length;
                    url = url.Substring(0, index2);

                    var arr1 = cookie.Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    foreach (var item1 in arr1)
                    {
                        var arr2 = item1.Split("=".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                        if (arr2.Length != 2) continue;
                        // 写入cookie
                        afterFormLoadArgs.BrowserImp.AddCookie(url, new Cookie()
                        {
                            Name = arr2[0].Trim(),
                            Path = "/",
                            Value = arr2[1].Trim(),
                            Expires = DateTime.MaxValue
                        });
                    }

                    afterFormLoadArgs.BrowserImp.Reload();
                }
            }
        }
    }
}
