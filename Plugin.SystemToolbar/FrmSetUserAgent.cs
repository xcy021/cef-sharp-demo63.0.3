﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Plugin.SystemToolbar
{
    public partial class FrmSetUserAgent : Form
    {
        public string UserAgent { get; set; }
        public FrmSetUserAgent()
        {
            InitializeComponent();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            this.UserAgent = this.textBox1.Text.Trim();
            this.DialogResult = DialogResult.OK;
        }
    }
}
