﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharpBrowser.Plugins;

namespace Plugin.Bookmarks
{
    class BookmarkPlugin : PluginAbstract
    {
        public override string PluginName { get; } = "收藏夹";

        public override void AfterFormLoad(AfterFormLoadArgs args)
        {
            base.AfterFormLoad(args);
            InitBookmarksToolbar(args);
        }

        public override void AfterAddressChanged(string address)
        {
            
        }

        /// <summary>
        /// 初始化收藏夹工具栏
        /// </summary>
        private void InitBookmarksToolbar(AfterFormLoadArgs args)
        {
            var toolbar = args.BrowserImp.CreateAndInsertToolStrip();
            var bookmarkRepo = new BookmarkRepoImpl();
            var bookmarks = bookmarkRepo.ReadBookmarks();
            foreach (var item in bookmarks)
            {
                ToolStripButton tsb = new ToolStripButton();
                tsb.ImageTransparentColor = System.Drawing.Color.Magenta;
                tsb.Size = new System.Drawing.Size(80, 24);
                tsb.Text = item.title;
                tsb.Click += (o, e) =>
                {
                    args.BrowserImp.LoadUrl(item.href);
                };
                toolbar.Items.Add(tsb);
            }
        }
    }
}
