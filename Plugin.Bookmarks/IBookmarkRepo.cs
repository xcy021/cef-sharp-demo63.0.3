﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plugin.Bookmarks
{
    interface IBookmarkRepo
    {
        List<Bookmark> ReadBookmarks();
        void WriteBookmarks(List<Bookmark> bookmarks);
        void AddBookmark(Bookmark bookmark);
        void RemoveBookmark(Bookmark bookmark);
        void UpdateBookmark(Bookmark bookmark);
    }
}
