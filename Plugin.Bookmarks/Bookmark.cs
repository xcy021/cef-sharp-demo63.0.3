﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plugin.Bookmarks
{
    class Bookmark
    {
        public string href { get; set; }
        public string title { get; set; }
        public DateTime add_date { get; set; }
    }
}
