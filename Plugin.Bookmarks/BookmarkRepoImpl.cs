﻿using CefSharpBrowser;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Plugin.Bookmarks
{
    class BookmarkRepoImpl : IBookmarkRepo
    {
        private string bookmarkFile;
        private string bookmarkDir;

        public BookmarkRepoImpl()
        {
            bookmarkDir = CefSharpBrowserRepo.GetPath("Bookmarks");
            if (!Directory.Exists(bookmarkDir))
                Directory.CreateDirectory(bookmarkDir);
            bookmarkFile = bookmarkDir + "\\bookmarks.json";
        }

        private void MakeBookmarksFileExists()
        {
            if (!File.Exists(bookmarkFile))
            {
                File.WriteAllBytes(bookmarkFile, Properties.Resources.bookmarks);
            }
        }

        public void AddBookmark(Bookmark bookmark)
        {
            var bookmarks = ReadBookmarks();
            bookmarks.Add(bookmark);
            WriteBookmarks(bookmarks);
        }

        public void RemoveBookmark(Bookmark bookmark)
        {
            var bookmarks = ReadBookmarks();
            bookmarks.RemoveAll(m => m.href == bookmark.href);
            WriteBookmarks(bookmarks);
        }

        public void UpdateBookmark(Bookmark bookmark)
        {
            var bookmarks = ReadBookmarks();
            var found = bookmarks.Find(m => m.href == bookmark.href);
            found.title = bookmark.title;
            found.add_date = bookmark.add_date;
            WriteBookmarks(bookmarks);
        }

        public List<Bookmark> ReadBookmarks()
        {
            MakeBookmarksFileExists();
            try
            {
                string json = File.ReadAllText(bookmarkFile);
                return json.FromJson<List<Bookmark>>();
            }
            catch (Exception ex)
            {
                return new List<Bookmark>();
            }
        }

        public void WriteBookmarks(List<Bookmark> bookmarks)
        {
            try
            {
                var json = bookmarks.ToJson();
                File.WriteAllText(bookmarkFile, json);

            }
            catch (Exception ex)
            {
            }
        }
    }
}
