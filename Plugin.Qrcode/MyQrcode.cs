﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CefSharpBrowser.Plugins;
using QRCoder;

namespace Plugin.Qrcode
{
    public class MyQrcode : PluginAbstract
    {
        public override string PluginName
        {
            get { return "二维码"; }
        }

        public override void AfterAddressChanged(string address)
        {
            if (true)
            {
                this.afterFormLoadArgs.BrowserImp.AddPluginItem(this.PluginName, (sender, args) =>
                {
                    var qrCodeImage = CreateQRCodeImage(address);
                    new Form1(qrCodeImage).ShowDialog();
                });
            }
        }

        private Bitmap CreateQRCodeImage(string text)
        {
            QRCodeGenerator qrGenerator = new QRCoder.QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(text, QRCodeGenerator.ECCLevel.Q);
            QRCode qrcode = new QRCode(qrCodeData);
            Bitmap qrCodeImage = qrcode.GetGraphic(5, Color.Black, Color.White, null, 15, 6, false);

            return qrCodeImage;
        }
    }
}
