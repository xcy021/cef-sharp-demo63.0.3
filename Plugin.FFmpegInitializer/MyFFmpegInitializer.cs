﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharpBrowser;
using CefSharpBrowser.Plugins;

namespace Plugin.Initializer
{
    public class MyFFmpegInitializer : PluginAbstract
    {
        private static bool IsInitialized = false;

        public override string PluginName { get; } = "FFmpegInitializer";

        public override void AfterFormLoad(AfterFormLoadArgs args)
        {
            if (IsInitialized)
                return;
            base.AfterFormLoad(args);
            CopyFfmpegFile();
            IsInitialized = true;
        }

        public override void AfterAddressChanged(string address)
        {
        }

        private static void CopyFfmpegFile()
        {
            /*
             *  \tools\annie\ffmpeg.exe
                \tools\N_m3u8DL-CLI\ffmpeg.exe
             */
            var toolsDir = CefSharpBrowserRepo.GetPath(AppPath.Tools);
            List<string> ffmpegList = new List<string>()
            {
                Path.Combine(toolsDir, "annie\\ffmpeg.exe"),
                Path.Combine(toolsDir, "N_m3u8DL-CLI\\ffmpeg.exe"),
            };
            string found = null;
            foreach (var file in ffmpegList)
            {
                if (File.Exists(file))
                {
                    found = file;
                    break;
                }
            }
            if (found == null)
            {
                MessageBox.Show("ffmpeg.exe文件丢失！");
                Environment.Exit(0);
            }
            foreach (var file in ffmpegList)
            {
                if (File.Exists(file))
                {
                    continue;
                }

                File.Copy(found, file);
            }
        }
    }
}
