# CefSharpDemo63.0.3

## 1. 介绍

### 1.1 环境

- VS2017
- .net framework版本4.5.2
- CefSharp 63.0.3
- 修改版的libcef.dll（已内置），支持H.264、MP3、AAC

### 1.2 功能
#### 1.2.1 基本功能
- 前进、后台、刷新、首页、地址栏
- 网页加载状态、网页地址变化、网页标题变化
- 支持F5刷新
- 支持F12打开开发者工具（解决F12弹出两个开发者工具问题）
- H.264 support、MP3 support、AAC support（可以在线播放腾讯视频、优酷视频等）
- 启用缓存（可以自动登录）
- 网页中js、css、png等资源替换（百度logo更换为搜狗logo、js替换等）
- 禁止弹出右键菜单
- 禁止弹出新窗口
- 右键菜单默认中文、网站默认中文
- 清除cookie（清除已登录的账号）
```c#
Cef.GetGlobalCookieManager().DeleteCookies("", "");
```
- c#对象注入到web、js调用c#、c#调用js
- 解决“网页里的元素点击位置与界面显示的问题不一致”问题
```c#
Cef.EnableHighDPISupport();
```
- 如果load()前后网站域名发生变化，注入到web中的c#对象仍可使用
```c#
CefSharpSettings.LegacyJavascriptBindingEnabled = true;
_browser.JavascriptObjectRepository.Register("myWin", new MyObjectRegisterToWeb());
```
改为
```c#
_browser.JavascriptObjectRepository.ResolveObject += (s, eve) =>
{
    var repo = eve.ObjectRepository;
    if (eve.ObjectName == "myWin")
    {
        repo.Register("myWin", new MyObjectRegisterToWeb(), isAsync: true, options: BindingOptions.DefaultBinder);
    }
};
```
- 地址栏输入非网址，可以直接采用百度搜索
- 关闭含有视频的tab页后，不会保留原视频声音
```c#
_browser?.Dispose();
```

#### 1.2.2 其他功能
- 支持多标签
- 支持快捷键：ctrl+t、ctrl+w、f5、f12
- 支持替换资源，并且刷新后可立即生效，无需重启浏览器或者采用F12-Network-勾选Disable Cache
```c#
_browser.GetBrowser().Reload(true);
```
- 支持cookie隔离，多个小号之间不影响（系统功能-打开小号窗口）
```c#
RequestContextSettings requestContextSettings = new RequestContextSettings();
requestContextSettings.PersistSessionCookies = false;
requestContextSettings.PersistUserPreferences = false;
_browser.RequestContext = new RequestContext(requestContextSettings);
```
- 支持全屏和取消全屏
```c#
// 实现接口函数
IDisplayHandler.OnFullscreenModeChange()
// 绑定接口实例
_browser.DisplayHandler = IDisplayHandler实例;
```

- 支持cookie登录网站（系统功能-加载cookie）

```c#
ICookieManager cookieManager = Cef.GetGlobalCookieManager();
cookieManager.SetCookie("https://www.baidu.com", new Cookie()
{
	Name = "xxxx",
	Path = "/",
	Value = "xxxx",
	Expires = DateTime.MaxValue
});
```



#### 1.2.3 插件系统

> 视频下载功能

- 集成N_m3u8DL-CLI工具，自动捕获m3u8文件地址，并支持调用N_m3u8DL-CLI进行下载，支持绝大部分采用m3u8播放的网站（各种第三方视频网站）
- 集成annie工具（用于下载腾讯、优酷、爱奇艺、bilibili等视频网站）
- 集成youtube地址解析插件savefrom_net、nsfwyoutube（针对必须登录才能下载的视频）

> 其他

- 支持网址二维码插件
- 支持钉钉防撤回、屏蔽消息已读功能
- 支持JsLoader插件，可在网页加载完成后加载任意js，类似于精简版油猴插件

## 2. 界面
![输入图片说明](https://images.gitee.com/uploads/images/2021/0706/231819_69bb7930_796560.png "Snipaste_2021-07-06_23-17-26.png")