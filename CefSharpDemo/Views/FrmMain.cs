﻿using System;
using System.Windows.Forms;
using CefSharp;
using CefSharpBrowser;
using CefSharpBrowser.Browser;
using CefSharpDemo.Extensions;
using FarsiLibrary.Win;

namespace CefSharpDemo.Views
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
            InitFaTabStripItemAdd();
            Load += Form1_Load;
            faTabStrip1.TabStripItemSelectionChanged += OnTabsChanged;
            faTabStrip1.MouseDoubleClick += faTabStrip1_MouseDoubleClick;
        }

        private void InitFaTabStripItemAdd()
        {
            faTabStripItemAdd.CanClose = false;
            faTabStripItemAdd.Title = "+";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            AddTab(CefSharpBrowserRepo.HomeUrl);
        }

        private void OnTabsChanged(TabStripItemChangedEventArgs e)
        {
            if (e.ChangeType == FATabStripItemChangeTypes.SelectionChanged)
            {
                if (faTabStrip1.SelectedItem == faTabStripItemAdd) AddTab(CefSharpBrowserRepo.HomeUrl);
            }
            else if (e.ChangeType == FATabStripItemChangeTypes.Removed)
            {
                ReleaseTabItemRes(e.Item);
            }
        }

        private void AddTab(string url)
        {
            this.RunOnUIAsync(() =>
            {
                var chromeWebBrowserUc = new ChromeWebBrowserUC(url, AddTab, RemoveTab, ChangeTabTitle);
                chromeWebBrowserUc.Dock = DockStyle.Fill;

                var tabStripItem = new FATabStripItem();
                tabStripItem.IsDrawn = true;
                tabStripItem.Selected = true;
                tabStripItem.Title = url;
                chromeWebBrowserUc.Tag = tabStripItem;
                tabStripItem.Controls.Add(chromeWebBrowserUc);

                faTabStrip1.Items.Insert(faTabStrip1.Items.Count - 1, tabStripItem);
                faTabStrip1.SelectedItem = tabStripItem;
            });
        }

        private void RemoveTab()
        {
            RemoveTab(this.faTabStrip1.SelectedItem);
        }

        private void RemoveTab(FATabStripItem tabItem)
        {
            this.RunOnUIAsync(() =>
            {
                if (faTabStrip1.Items.Count >= 2)
                {
                    int index = faTabStrip1.Items.IndexOf(tabItem);
                    faTabStrip1.Items.Remove(tabItem);
                    ReleaseTabItemRes(tabItem);
                    index = index - 1;
                    if (index < 0)
                        index = 0;
                    faTabStrip1.SelectedItem = faTabStrip1.Items[index];
                }
            });
        }

        /// <summary>
        ///     释放Tab页中的浏览器资源
        /// </summary>
        /// <param name="tabItem"></param>
        private void ReleaseTabItemRes(FATabStripItem tabItem)
        {
            for (var i = 0; i < tabItem.Controls.Count; i++)
            {
                var ctrl = tabItem.Controls[i];
                if (ctrl is ChromeWebBrowserUC)
                {
                    (ctrl as ChromeWebBrowserUC).Dispose();
                    break;
                }
            }
        }

        /// <summary>
        /// 修改tab标题
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="title"></param>
        private void ChangeTabTitle(object tag, string title)
        {
            var item = tag as FATabStripItem;
            item.Title = title;
        }

        /// <summary>
        /// 双击tab关闭网页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void faTabStrip1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            var item = faTabStrip1.GetTabItemByPoint(e.Location);
            if(item != null && item != faTabStripItemAdd)
                RemoveTab(item);
        }

        //private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        //{
        //    var handler = _browser.RequestHandler as IMyResReplaceRequestHandler;
        //    handler.SaveReplaceRule();
        //}
    }
}