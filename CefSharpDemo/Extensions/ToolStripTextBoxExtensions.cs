﻿using System;
using System.Windows.Forms;

namespace CefSharpDemo.Extensions
{
    public static class ToolStripTextBoxExtensions
    {
        public static void OnEnterPressed(this ToolStripTextBox c, Action<ToolStripTextBox> action)
        {
            c.KeyPress += (sender, e) =>
            {
                if (e.KeyChar == (char) Keys.Enter)
                {
                    action(sender as ToolStripTextBox);
                    e.Handled = true;
                }
            };
        }

        public static void OnEnterPressed(this TextBox c, Action<TextBox> action)
        {
            c.KeyPress += (sender, e) =>
            {
                if (e.KeyChar == (char) Keys.Enter)
                {
                    action(sender as TextBox);
                    e.Handled = true;
                }
            };
        }
    }
}