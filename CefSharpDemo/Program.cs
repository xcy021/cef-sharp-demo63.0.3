﻿using System;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using CefSharpBrowser;
using CefSharpDemo.Views;

namespace CefSharpDemo
{
    internal static class Program
    {
        /// <summary>
        ///     应用程序的主入口点。
        /// </summary>
        [STAThread]
        private static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            CefSharpBrowserRepo.AllowMultiAccountCookie = false;
            var cefInitializer = CefSharpBrowserRepo.CreateCefInitializer();
            foreach (var arg in args)
            {
                if (arg.StartsWith("/AllowMultiAccountCookie:"))
                {
                    var v = arg.Substring("/AllowMultiAccountCookie:".Length);
                    bool v2 = bool.Parse(v);
                    CefSharpBrowserRepo.AllowMultiAccountCookie = v2;
                }
                else if (arg.StartsWith("/SetUserAgent:"))
                {
                    var v = arg.Substring("/SetUserAgent:".Length);
                    if(!string.IsNullOrWhiteSpace(v))
                    {
                        cefInitializer.UserAgent = v;
                    }
                }
            }
            cefInitializer.InitializeCefSettings();
            var frm = new FrmMain();
            frm.Load += (o, e) =>
            {
                IMyFormIconHooker iconHooker = new MyFormIconHooker();
                iconHooker.Hook("icons8_chrome_256.ico");
                if (CefSharpBrowserRepo.AllowMultiAccountCookie)
                {
                    frm.Text += " [小号模式]";
                }
            };

            Application.Run(frm);
        }

        private interface IMyFormIconHooker
        {
            void Hook(string resName);
        }

        private class MyFormIconHooker : IMyFormIconHooker
        {
            private Icon _resIcon;

            public void Hook(string resName)
            {
                _resIcon = GetResAsIcon(resName);
                var threadId = GetCurrentThreadId();
                Console.WriteLine("hook thread id:" + threadId);
                EnumThreadWindows(threadId, EnumThreadWindowsCallback, IntPtr.Zero);
            }

            [DllImport("user32.dll", ExactSpelling = true)]
            private static extern bool EnumThreadWindows(int dwThreadId, EnumThreadWindowsDelegate lpfn, IntPtr lParam);

            [DllImport("kernel32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
            public static extern int GetCurrentThreadId();

            private bool EnumThreadWindowsCallback(IntPtr handle, IntPtr lParam)
            {
                var control = Control.FromHandle(handle);
                var flag = control != null;
                if (flag)
                {
                    var flag2 = control is Form;
                    if (flag2) (control as Form).Icon = _resIcon;
                }

                return true;
            }

            private Icon GetResAsIcon(string resName)
            {
                var manifestResourceNames = Assembly.GetEntryAssembly().GetManifestResourceNames().ToList();
                var resNameFull = manifestResourceNames.ToList().Find(m => m.Contains(resName));
                var stream = Assembly.GetEntryAssembly().GetManifestResourceStream(resNameFull);
                return new Icon(stream);
            }

            private delegate bool EnumThreadWindowsDelegate(IntPtr hwnd, IntPtr lParam);
        }
    }
}